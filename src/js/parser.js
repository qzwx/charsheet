import character from "./character.js";
import {MODIFIERS} from "./util.js";

// https://matklad.github.io/2020/04/13/simple-but-powerful-pratt-parsing.html

function isWhiteSpace(ch) {
	return /^\s$/.test(ch);
}

function isDigit(ch) {
	return /^\d$/.test(ch);
}

function isLetter(ch) {
	return /^[a-zñáéíóúàèìòùäëïöüħðþåãøłæœ]$/i.test(ch);
}

function isWordChar(ch) {
	return /^[a-zñáéíóúàèìòùäëïöüħðþåãøłæœ0-9_-]$/i.test(ch);
}

function isQuote(ch) {
	return /^[`'"]$/i.test(ch);
}

class InputStream {
	constructor(input) {
		this.idx = 0;
		this.input = input;
	}

	peek() {
		return this.input.charAt(this.idx);
	}

	next() {
		return this.input.charAt(this.idx++);
	}

	eof() {
		return this.peek() === "";
	}
}

class Token {
	constructor(type, value, pos) {
		this.type = type;
		this.value = value;
		this.pos = pos;
	}
}

class Tokenizer {
	constructor(inputStream) {
		if (inputStream instanceof InputStream) {
			this.inputStream = inputStream;
		} else {
			this.inputStream = new InputStream(inputStream);
		}
	}

	readWhile(test) {
		var s = "";
		while (test(this.inputStream.peek())) s += this.inputStream.next();
		return s;
	}

	readNumber() {
		let pos = this.inputStream.idx;
		let n = this.readWhile(isDigit);
		if (this.inputStream.peek() === ".") {
			n += this.inputStream.next();
			n += this.readWhile(isDigit);
		}
		return new Token("number", n, pos);
	}

	readWord() {
		// only the first char *has* to be a letter
		let pos = this.inputStream.idx;
		let w = this.readWhile(isWordChar);
		return new Token("identifier", w, pos);
	}

	readString() {
		let pos = this.inputStream.idx;
		let startQuote = this.inputStream.next();
		let s = this.readWhile((ch) => ch !== startQuote);
		let endQuote = this.inputStream.next();
		return new Token("string", s, pos);
	}

	readNext() {
		this.readWhile(isWhiteSpace);
		if (this.inputStream.eof()) return null;
		let pos = this.inputStream.idx;
		let ch = this.inputStream.peek();
		if (isDigit(ch)) {
			return this.readNumber();
		} else if (isLetter(ch)) {
			return this.readWord();
		} else if (isQuote(ch)) {
			return this.readString();
		} else {
			return new Token("operator", this.inputStream.next(), pos);
		}
	}

	peek() {
		return this.current || (this.current = this.readNext());
	}

	next() {
		//console.log('Tokenizer.next');
		var tok = this.current;
		this.current = null;
		return tok || this.readNext();
	}

	eof() {
		return this.peek() === null;
	}
}

class Cons {
	constructor(token, args) {
		this.token = token;
		this.args = args;
	}

	get type() {
		return "cons";
	}

	get repr() {
		return `(${this.token.value} ${this.args
			.map((a) => (a && a.repr) || "undefined")
			.join(" ")})`;
	}
}

class UnaryOp extends Cons {
	execute(ctx) {
		// console.log("execute UnaryOp", this.token.value);
		if (this.token.value === "-") {
			return -this.args[0].execute(ctx);
		} else if (this.token.value === "+") {
			return this.args[0].execute(ctx);
		}
	}
}

class BinaryOp extends Cons {
	execute(ctx) {
		// console.log("execute BinaryOp", this.token.value);
		if (this.token.value === "*") {
			return this.args[0].execute(ctx) * this.args[1].execute(ctx);
		} else if (this.token.value === "/") {
			return this.args[0].execute(ctx) / this.args[1].execute(ctx);
		} else if (this.token.value === "+") {
			return this.args[0].execute(ctx) + this.args[1].execute(ctx);
		} else if (this.token.value === "-") {
			return this.args[0].execute(ctx) - this.args[1].execute(ctx);
		}
	}
}

class DieRoll extends Cons {
	execute(ctx) {
		let n = this.args[0].execute(ctx);
		let d = this.args[1].execute(ctx);
		let s = 0;
		let rands = new Uint8Array(n);
		crypto.getRandomValues(rands);
		for (let i = 0; i < n; ++i) {
			let roll = Math.floor((rands[i] / 256) * d) + 1;
			if (ctx.rolls) {
				ctx.rolls.push({die: d, value: roll});
			}
			s += roll;
		}
		return s;
	}
}

const mathFns = new Set([
	"min",
	"max",
	"abs",
	"sqrt",
	"pow",
	"floor",
	"round",
	"ceil"
]);

class FnCall extends Cons {
	execute(ctx) {
		// console.log("execute FnCall", this.token.value);
		if (mathFns.has(this.token.value)) {
			return Math[this.token.value].apply(
				{},
				this.args.map((a) => a.execute(ctx))
			);
		} else if (this.token.value === "level" || this.token.value === "lvl") {
			let arg = undefined;
			if (this.args.length > 0 && this.args[0].type === "string") {
				arg = this.args[0].value;
			}
			return ctx.getLevel(arg);
		} else if (
			this.token.value === "proficiencyBonus" ||
			this.token.value === "pb"
		) {
			return ctx.getProficiencyBonus();
		} else if (this.token.value === "expertise" || this.token.value === "ex") {
			return 2 * ctx.getProficiencyBonus();
		} else if (this.token.value === "mod" || this.token.value === "m") {
			return parseFloat(MODIFIERS[Math.min(this.args[0].execute(ctx), 30)]);
		} else if (this.token.value === "sign" || this.token.value === "s") {
			let v = this.args[0].execute(ctx);
			return v >= 0 ? "+" + parseFloat(v) : v;
		} else if (this.token.value === "if") {
			return this.args[0].execute(ctr)
				? this.args[1].execute(ctr)
				: this.args[2].execute(ctr);
		}
		throw new Error(`Unknown function ${this.token.value}`);
	}
}

class Atom {
	constructor(token) {
		this.token = token;
	}

	get type() {
		return this.token.type;
	}

	get repr() {
		return this.token.value;
	}

	execute(ctx) {
		// console.log("execute Atom", this.token.value);
		if (this.type === "number") {
			return parseFloat(this.token.value);
		} else if (
			this.type === "identifier" &&
			ctx.hasOwnProperty(this.token.value)
		) {
			return parseFloat(merge(ctx[this.token.value]));
		}
		throw new Error(`Invalid identifier "${this.token.value}"`);
	}
}

function infixBindingPower(token) {
	switch (token.value) {
		case "*":
		case "/":
			return {leftBp: 3, rightBp: 4};
		case "+":
		case "-":
			return {leftBp: 1, rightBp: 2};
		case ",":
		case ")":
			return null;
		default:
			throw new Error(
				`Unrecognized infix operator ${token.value} at position ${token.pos}`
			);
	}
}

function prefixBindingPower(token) {
	switch (token.value) {
		case "+":
		case "-":
			return 5;
		default:
			throw new Error(
				`Unrecognized prefix operator ${token.value} at position ${token.pos}`
			);
	}
}

function parse(input) {
	let tokenizer = new Tokenizer(input);
	let ast = expr(tokenizer, 0);
	if (!tokenizer.eof()) {
		let idx = tokenizer.peek().pos;
		throw new Error(
			`Parsing ended before consuming all input. Terminated at position ${idx}. Unconsumed: "${input.substring(
				idx
			)}"`
		);
	}
	return ast;
}

function expr(tokenizer, minBp) {
	let lht = tokenizer.next();
	let lhs = null;
	//console.log("lht", lht);
	if (lht.type === "number" || lht.type === "identifier" || lht.type === "string") {
		lhs = new Atom(lht);
	} else if (lht.type === "operator" && lht.value === "(") {
		lhs = expr(tokenizer, 0);
		let closeParen = tokenizer.next();
		if (closeParen.type !== "operator" || closeParen.value !== ")") {
			throw new Error(
				`Unexpected token "${closeParen.value}", expected ")" at position ${closeParen.pos}`
			);
		}
	} else if (lht.type === "operator") {
		lhs = new UnaryOp(lht, [expr(tokenizer, prefixBindingPower(lht))]);
	} else {
		throw new Error(`Bad token "${lht.value}" at position ${lht.pos}`);
	}
	//console.log("lhs", lhs);

	while (!tokenizer.eof()) {
		let op = tokenizer.peek();
		//console.log("op", op);
		if (op === null) {
			break;
		}

		if (
			lht.type === "identifier" &&
			op.type === "operator" &&
			op.value === "("
		) {
			// function call
			tokenizer.next(); // consume (
			let args = [];
			let sep = tokenizer.peek();
			while (sep.value !== ")") {
				args.push(expr(tokenizer, 0));
				sep = tokenizer.peek();
				if (
					sep.type !== "operator" ||
					(sep.value !== ")" && sep.value !== ",")
				) {
					throw new Error(
						`Unexpected token "${sep.value}", expected "," or ")" at position ${sep.pos}`
					);
				}
				if (sep.value === ",") {
					tokenizer.next(); // consume ,
				}
			}
			tokenizer.next(); // consume )
			lhs = new FnCall(lht, args);
			continue;
		}
		if (op.type === "identifier" && op.value.match(dExpr)) {
			tokenizer.next(); // consume op
			let die = parseInt(op.value.match(dExpr)[1]);
			lhs = new DieRoll(op, [lhs, new Atom({type: "number", value: die})]);
			continue;
		}
		let bps = infixBindingPower(op);
		if (!bps) {
			break;
		}
		let {leftBp, rightBp} = bps;
		if (leftBp < minBp) {
			break;
		}
		tokenizer.next(); // consume op
		let rhs = expr(tokenizer, rightBp);
		lhs = new BinaryOp(op, [lhs, rhs]);
	}

	return lhs;
}

const dExpr = /^d(\d+)$/i;
const exp = /\$\{([^}]+)\}/g;
function merge(input, key) {
	// console.log("merge", key, input);
	let char = character.getChar();
	if (typeof input === "number") {
		return input;
	} else if (typeof input === "string") {
		return input.replace(exp, (m, g1) => {
			// console.log("replacing", m);
			try {
				return parse(g1).execute(char);
			} catch (err) {
				// console.log("error in %s", key);
				console.error(err);
				return m;
			}
		});
	} else {
		throw new Error("Can't process input " + JSON.serialize(input));
	}
}

export {parse, merge};
