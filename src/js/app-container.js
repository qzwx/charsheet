import "./character-list.js";
import "./resets.js";
import "./character-sheet.js";
import "./dicetray.js";
import {fireEvent} from "./event.js";

export default class AppContainer extends HTMLElement {
	constructor() {
		super();
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
<link rel="stylesheet" href="/css/style.css" type="text/css"/>
<h2><cs-icon icon-name="d20" alternative-text="d20" size="24"></cs-icon> Character Sheet <cs-icon icon-name="d20" alternative-text="d20" size="24"></cs-icon></h2>
<div class="noprint">
	<cs-collapsible label="Dice Tray">
		<cs-dicetray></cs-dicetray>
	</cs-collapsible>
</div>
<div class="noprint">
	<cs-character-list></cs-character-list>
</div>
<div class="noprint">
	<cs-resets></cs-resets>
</div>
<div>
	<cs-character-sheet></cs-character-sheet>
</div>
`;
		this.handleListChange = this.handleListChange.bind(this);
		this.handleSheetChange = this.handleSheetChange.bind(this);
		this.handleKeyDown = this.handleKeyDown.bind(this);
	}

	connectedCallback() {
		this.charList = this.shadowRoot.querySelector("cs-character-list");
		this.charList.addEventListener("change", this.handleListChange);
		this.charSheet = this.shadowRoot.querySelector("cs-character-sheet");
		this.charSheet.addEventListener("change", this.handleSheetChange);
		document.addEventListener("keydown", this.handleKeyDown);
	}
	disconnectedCallback() {
		document.removeEventListener("keydown", this.handleKeyDown);
	}

	handleListChange() {
		this.charSheet.reload();
	}
	handleSheetChange() {
		this.charList.updateCharList();
	}
	handleKeyDown(evt) {
		//console.log("handleKeyDown", evt);
		if (evt.altKey && evt.keyCode === 188) {
			// ,
			fireEvent("open-all");
			evt.preventDefault();
		}
		if (evt.altKey && evt.keyCode === 190) {
			// .
			fireEvent("close-all");
			evt.preventDefault();
		}
		if (evt.altKey && evt.keyCode === 65) {
			// A
			fireEvent("toggle-actions", {shift: evt.shiftKey});
			evt.preventDefault();
		}
		if (evt.altKey && evt.keyCode === 68) {
			// D
			fireEvent("toggle-desc", {shift: evt.shiftKey});
			evt.preventDefault();
		}
		if (evt.altKey && evt.keyCode === 73) {
			// I
			fireEvent("toggle-inventory", {shift: evt.shiftKey});
			evt.preventDefault();
		}
		if (evt.altKey && evt.keyCode === 77) {
			// M
			fireEvent("toggle-magic", {shift: evt.shiftKey});
			evt.preventDefault();
		}
		if (evt.altKey && evt.keyCode === 78) {
			// N
			fireEvent("toggle-notes", {shift: evt.shiftKey});
			evt.preventDefault();
		}
		if (evt.altKey && evt.keyCode === 83) {
			// S
			fireEvent("toggle-stats", {shift: evt.shiftKey});
			evt.preventDefault();
		}
	}
}

customElements.define("cs-app-container", AppContainer);
