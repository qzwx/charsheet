import "./icon.js";

// prettier-ignore
const MODIFIERS = [
	null,
	-5,     // 1
	-4, -4, // 2-3
	-3, -3, // 4-5
	-2, -2, // 6-7
	-1, -1, // 8-9
	+0, +0, // 10-11
	+1, +1, // 12-13
	+2, +2, // 14-15
	+3, +3, // 16-17
	+4, +4, // 18-19
	+5, +5, // 20-21
	+6, +6, // 22-23
	+7, +7, // 24-25
	+8, +8, // 26-27
	+9, +9, // 28-29
	+10     // 30
];

function uuidv4() {
	return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (c) =>
		(
			c ^
			(crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
		).toString(16)
	);
}

function nvl(v1, v2) {
	if (v1 === null || v1 === undefined) {
		return v2;
	}
	return v1;
}

function addIcon(parent, name, alt, className) {
	let icon = document.createElement("cs-icon");
	icon.setAttribute("icon-name", name);
	icon.setAttribute("alternative-text", alt);
	if (className) {
		icon.className = className;
	}
	parent.appendChild(icon);
	return icon;
}
function addText(parent, text) {
	let span = document.createElement("span");
	span.textContent = text;
	parent.appendChild(span);
	return span;
}
function clearContent(el) {
	while (el.hasChildNodes()) {
		el.removeChild(el.lastChild);
	}
}

function hide(el) {
	el.classList.remove("show");
	el.classList.add("hide");
}
function show(el) {
	el.classList.remove("hide");
	el.classList.add("show");
}

function getElValue(selector) {
	let el = this.shadowRoot.querySelector(selector);
	if (el.tagName === "INPUT" && el.type === "checkbox") {
		return el.checked;
	}
	return el.value;
}
function setElValue(selector, value) {
	let el = this.shadowRoot.querySelector(selector);
	if (el.tagName === "INPUT" && el.type === "checkbox") {
		el.checked = value;
	}
	el.value = value;
}

function fixLabels() {
	let inputs = new Map();
	this.shadowRoot.querySelectorAll(".input").forEach((el) => {
		if (el.dataset.field) {
			inputs.set(el.dataset.field, el);
		}
	});
	this.shadowRoot.querySelectorAll("label").forEach((el) => {
		let input = inputs.get(el.dataset.for);
		if (input) {
			let id = uuidv4();
			input.setAttribute("id", id);
			el.setAttribute("for", id);
		}
	});
}

function countRows(value) {
	if (!value) {
		return 0;
	}
	let cnt = 0;
	let idx = 0;

	while (true) {
		idx = value.indexOf("\n", idx);
		if (idx === -1) {
			break;
		}
		++cnt;
		++idx;
	}
	return cnt;
}

export {
	addIcon,
	addText,
	clearContent,
	countRows,
	fixLabels,
	getElValue,
	hide,
	nvl,
	setElValue,
	show,
	uuidv4,
	MODIFIERS
};
