import {show, hide, clearContent} from "./util.js";

export default class LevelInput extends HTMLElement {
	_value = [];

	get value() {
		return this._value;
	}
	set value(val) {
		if (!(val instanceof Array)) {
			val = [];
		}
		val.sort((a, b) => a.class.localeCompare(b.class));
		this._value = val;
		this.updateList();
	}
	constructor() {
		super();
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
		<link rel="stylesheet" href="/css/style.css" type="text/css"/>
		<div class="cs-level-input">
			<div><label></label> <a class="edit-btn noprint"><cs-icon icon-name="pencil" alternative-text="edit"></cs-icon></a><a class="done-btn noprint hide"><cs-icon icon-name="pin" alternative-text="done"></cs-icon></a></div>
			<div class="class-list">
			</div>
			<div class="add-row hide">
				<div class="row noprint">
					<input class="col class-input mh" type="text"></input>
					<button class="col add-btn mh">Add Class</button>
				</div>
			</div>
		</div>
		`;
		this.addClass = this.addClass.bind(this);
		this.clickEdit = this.clickEdit.bind(this);
		this.doneEdit = this.doneEdit.bind(this);
		this.load = this.load.bind(this);
		this.lvlDown = this.lvlDown.bind(this);
		this.lvlUp = this.lvlUp.bind(this);
		this.signalChanged = this.signalChanged.bind(this);
		this.updateList = this.updateList.bind(this);
	}
	connectedCallback() {
		this.listDiv = this.shadowRoot.querySelector("div.class-list");
		this.classInput = this.shadowRoot.querySelector("input.class-input");
		this.editBtn = this.shadowRoot.querySelector("a.edit-btn");
		this.doneBtn = this.shadowRoot.querySelector("a.done-btn");
		this.addRow = this.shadowRoot.querySelector("div.add-row");
		this.addBtn = this.shadowRoot.querySelector("button.add-btn");
		this.addBtn.addEventListener("click", this.addClass);
		this.editBtn.addEventListener("click", this.clickEdit);
		this.doneBtn.addEventListener("click", this.doneEdit);
		this.shadowRoot.querySelector("label").textContent = this.getAttribute(
			"label"
		);
		this.key = this.getAttribute("key");
		this.mode = "output";
	}

	clickEdit() {
		this.mode = "input";
		show(this.addRow);
		show(this.doneBtn);
		hide(this.editBtn);
		this.shadowRoot.querySelectorAll("a.btn").forEach((itm) => {
			show(itm);
		});
	}

	doneEdit() {
		hide(this.addRow);
		hide(this.doneBtn);
		show(this.editBtn);
		this.shadowRoot.querySelectorAll("a.btn").forEach((itm) => {
			hide(itm);
		});
		this.signalChanged();
		this.mode = "output";
	}

	load(parent) {
		let val = parent[this.key];
		if (val != undefined) {
			this.value = val;
		} else {
			this.value = [];
		}
	}

	addClass() {
		let cls = this.classInput.value.trim();
		if (!cls) {
			return;
		}
		this.value.push({class: cls, level: 1});
		this.value = this.value.sort((a, b) => a.class.localeCompare(b.class));
	}

	signalChanged() {
		this.dispatchEvent(
			new CustomEvent("change", {
				detail: [{key: this.key, value: this.value}]
			})
		);
	}

	lvlUp(evt) {
		let idx = evt.currentTarget.parentNode.dataset.idx;
		this.value[idx].level += 1;
		this.updateList();
	}

	lvlDown(evt) {
		let idx = evt.currentTarget.parentNode.dataset.idx;
		this.value[idx].level -= 1;
		this.value = this.value.filter((classlvl) => classlvl.level > 0);
		// updateList is called by setter
	}

	updateList() {
		let val = this.value;
		// TODO only process relevant changes, leave elements that don't have to change
		clearContent(this.listDiv);
		val.forEach((classlevel, idx) => {
			let classDiv = document.createElement("div");
			classDiv.className = "row spread box mvxs";
			let classSpan = document.createElement("span");
			classSpan.textContent = classlevel.class;
			classDiv.appendChild(classSpan);
			let lvlDiv = document.createElement("div");
			lvlDiv.dataset.idx = idx;
			classDiv.appendChild(lvlDiv);
			let lvlSpan = document.createElement("span");
			lvlSpan.className = "mh1";
			lvlSpan.textContent = classlevel.level;
			let plusBtn = document.createElement("a");
			plusBtn.textContent = "⊕";
			plusBtn.classList.add("btn");
			let minBtn = document.createElement("a");
			minBtn.textContent = "⊖";
			minBtn.classList.add("btn");
			plusBtn.addEventListener("click", this.lvlUp);
			minBtn.addEventListener("click", this.lvlDown);
			if (this.mode === "output") {
				plusBtn.classList.add("hide");
				minBtn.classList.add("hide");
			}
			lvlDiv.appendChild(plusBtn);
			lvlDiv.appendChild(lvlSpan);
			lvlDiv.appendChild(minBtn);
			this.listDiv.appendChild(classDiv);
		});
	}
}
customElements.define("cs-level-input", LevelInput);
