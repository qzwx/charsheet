import {countRows} from "./util.js";
import {merge} from "./parser.js";
import {registerListener, deregisterListener} from "./event.js";

export default class TextArea extends HTMLElement {
	_value = "";

	get value() {
		return this._value;
	}
	set value(val) {
		this._value = val;
		if (this.input) {
			this.input.value = val;
			this.input.setAttribute(
				"rows",
				Math.max(this.getAttribute("rows") || 5, countRows(this.value))
			);
		}
		if (this.output) {
			this.output.textContent = merge(val, this.key);
		}
	}
	constructor() {
		super();
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
		<link rel="stylesheet" href="/css/style.css" type="text/css"/>
		<div>
		<div><label></label> <a class="edit-btn noprint"><cs-icon icon-name="pencil" alternative-text="edit"></cs-icon></a><a class="done-btn noprint hide"><cs-icon icon-name="pin" alternative-text="done"></cs-icon></a></div>
		<div class="output box pre"></div><textarea class="input hide" type="text"></textarea>
		</div>
		`;
		this.clickEdit = this.clickEdit.bind(this);
		this.doneEdit = this.doneEdit.bind(this);
		this.load = this.load.bind(this);
		this.recalcCallback = this.recalcCallback.bind(this);
	}
	connectedCallback() {
		this.input = this.shadowRoot.querySelector("textarea.input");
		this.output = this.shadowRoot.querySelector("div.output");
		this.editBtn = this.shadowRoot.querySelector("a.edit-btn");
		this.doneBtn = this.shadowRoot.querySelector("a.done-btn");
		this.editBtn.addEventListener("click", this.clickEdit);
		this.input.addEventListener("blur", this.doneEdit);
		this.shadowRoot.querySelector("label").textContent = this.getAttribute(
			"label"
		);
		this.key = this.getAttribute("key");
		this.input.value = this.value;
		this.output.textContent = merge(this.value, this.key);
		this.mode = "output";
		registerListener("recalc", this.recalcCallback);
	}

	disconnectedCallback() {
		deregisterListener("recalc", this.recalcCallback);
	}

	recalcCallback() {
		this.output.textContent = merge(this.value, this.key);
	}

	attributeChangedCallback(name, oldVal, newVal) {
		if (name === "label") {
			this.shadowRoot.querySelector("label").textContent = newVal;
		}
	}

	clickEdit() {
		this.mode = "input";
		this.input.classList.remove("hide");
		this.input.classList.add("show");
		this.doneBtn.classList.remove("hide");
		this.doneBtn.classList.add("show");
		this.output.classList.add("hide");
		this.output.classList.remove("show");
		this.editBtn.classList.add("hide");
		this.editBtn.classList.remove("show");
		this.input.focus();
	}

	doneEdit() {
		this.value = this.input.value;
		this.input.setAttribute(
			"rows",
			Math.max(this.getAttribute("rows") || 5, countRows(this.value))
		);
		this.output.textContent = this.value;
		this.mode = "output";
		this.output.classList.remove("hide");
		this.output.classList.add("show");
		this.editBtn.classList.remove("hide");
		this.editBtn.classList.add("show");
		this.input.classList.add("hide");
		this.input.classList.remove("show");
		this.doneBtn.classList.add("hide");
		this.doneBtn.classList.remove("show");
		this.dispatchEvent(
			new CustomEvent("change", {detail: [{key: this.key, value: this.value}]})
		);
	}

	load(parent) {
		let val = parent[this.key];
		if (val != undefined) {
			this.value = val;
		} else {
			this.value = "";
		}
	}
}
customElements.define("cs-textarea", TextArea);
