import "./collapsible.js";
import character from "./character.js";
import {addIcon, clearContent} from "./util.js";

export default class CharacterList extends HTMLElement {
	constructor() {
		super();
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
<link rel="stylesheet" href="/css/style.css" type="text/css"/>
<div class="box noprint">
	<div class="row">
		<button class="col mh btn load-btn">Load</button>
		<button class="col mh btn save-btn">Save</button>
	</div>
	<cs-collapsible class="char-list-collapsible" label="Characters" mode="closed">
		<div class="char-list">
		</div>
		<button class="btn new-char-btn w100">New Character</button>
	</cs-collapsible>
</div>
<cs-modal class="load-modal">
	<h2 slot="header">Load File</h2>
	<div class="mh mv">
		<input class="load-file-input" type="file" accept="application/json">
		<button class="btn modal-load-btn">load</button>
	</div>
</cs-modal>`;
		this.deleteChar = this.deleteChar.bind(this);
		this.load = this.load.bind(this);
		this.loadFile = this.loadFile.bind(this);
		this.newChar = this.newChar.bind(this);
		this.save = this.save.bind(this);
		this.selectChar = this.selectChar.bind(this);
		this.updateCharList = this.updateCharList.bind(this);
	}

	connectedCallback() {
		this.shadowRoot
			.querySelector("button.load-btn")
			.addEventListener("click", this.load);
		this.shadowRoot
			.querySelector("button.save-btn")
			.addEventListener("click", this.save);
		this.shadowRoot
			.querySelector("button.modal-load-btn")
			.addEventListener("click", this.loadFile);
		this.shadowRoot
			.querySelector("button.new-char-btn")
			.addEventListener("click", this.newChar);

		this.updateCharList();
	}

	newChar() {
		character.newChar();
		this.dispatchEvent(new CustomEvent("change"));
		this.updateCharList();
	}

	save() {
		let char = character.getChar();
		let file = new Blob([JSON.stringify(char, null, "\t")], {
			type: "application/json"
		});
		let a = document.createElement("a");
		let url = URL.createObjectURL(file);
		a.href = url;
		a.download = `${char.name ? char.name : "character"}.json`;
		document.body.appendChild(a);
		a.click();
		setTimeout(function () {
			document.body.removeChild(a);
			window.URL.revokeObjectURL(url);
		}, 0);
	}

	load() {
		this.shadowRoot.querySelector("cs-modal.load-modal").openModal();
	}

	async loadFile() {
		let fileEl = this.shadowRoot.querySelector("input.load-file-input");
		if (fileEl.files.length < 1) {
			console.log("No file chosen..");
			return;
		}
		let f = fileEl.files[0];
		await character.readCharFromFile(f);
		this.dispatchEvent(new CustomEvent("change"));
		this.updateCharList();
		this.shadowRoot.querySelector("cs-modal.load-modal").closeModal();
	}

	selectChar(evt) {
		let uuid = evt.currentTarget.parentNode.dataset.uuid;
		console.log("selectChar", uuid);
		character.selectChar(uuid);
		this.dispatchEvent(new CustomEvent("change"));
	}

	deleteChar(evt) {
		let uuid = evt.currentTarget.parentNode.dataset.uuid;
		console.log("deletechar", uuid);
		character.deleteChar(uuid);
		this.updateCharList();
	}

	updateCharList() {
		let listDiv = this.shadowRoot.querySelector(".char-list");
		// TODO only process relevant changes, leave elements that don't have to change
		clearContent(listDiv);
		let charList = character.getCharList();
		//console.log("updateCharList", charList);
		let chars = Object.keys(charList)
			.filter((key) => charList.hasOwnProperty(key))
			.map((key) => charList[key]);
		chars.sort((a, b) => a.name.localeCompare(b.name));
		chars.forEach((char) => {
			let charDiv = document.createElement("div");
			charDiv.className = "pv row spread";
			charDiv.dataset.uuid = char.uuid;
			let nameDiv = document.createElement("div");
			nameDiv.textContent = char.name;
			charDiv.appendChild(nameDiv);
			let btn = document.createElement("a");
			btn.className = "btn del-char-btn";
			addIcon(btn, "bin", "remove");
			charDiv.appendChild(btn);
			nameDiv.addEventListener("click", this.selectChar);
			btn.addEventListener("click", this.deleteChar);
			listDiv.appendChild(charDiv);
		});
	}
}

customElements.define("cs-character-list", CharacterList);
