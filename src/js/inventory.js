import "./inventory-slot.js";
import {show, hide, addIcon, fixLabels, uuidv4, clearContent} from "./util.js";

export default class Inventory extends HTMLElement {
	_value = {};

	get value() {
		return this._value;
	}
	set value(val) {
		this._value = val;
		this.updateList();
	}
	constructor() {
		super();
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
		<link rel="stylesheet" href="/css/style.css" type="text/css"/>
		<div class="cs-inventory">
			<div class="btn-container"><a class="edit-btn noprint"><cs-icon icon-name="pencil" alternative-text="edit"></cs-icon></a><a class="done-btn noprint hide"><cs-icon icon-name="pin" alternative-text="done"></cs-icon></a></div>
			<div class="weight-output secondary-text"></div>
			<div class="output"></div>
			<div class="input noprint hide mt">
				<div><label data-for="slot-name">Add slot</label></div>
				<div class="row">
					<input class="mh mv col name-input" data-field="slot-name" type="text"></input>
					<button class="mh mv col add-btn">Add</button>
				</div>
			</div>
		</div>
		`;
		this.addSlot = this.addSlot.bind(this);
		this.calcWeight = this.calcWeight.bind(this);
		this.clickEdit = this.clickEdit.bind(this);
		this.doneEdit = this.doneEdit.bind(this);
		this.fixLabels = fixLabels.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.load = this.load.bind(this);
		this.remSlot = this.remSlot.bind(this);
		this.updateList = this.updateList.bind(this);
	}
	connectedCallback() {
		this.input = this.shadowRoot.querySelector("div.input");
		this.output = this.shadowRoot.querySelector("div.output");
		this.editBtn = this.shadowRoot.querySelector("a.edit-btn");
		this.doneBtn = this.shadowRoot.querySelector("a.done-btn");
		this.addBtn = this.shadowRoot.querySelector("button.add-btn");
		this.nameInput = this.shadowRoot.querySelector("input.name-input");
		this.editBtn.addEventListener("click", this.clickEdit);
		this.doneBtn.addEventListener("click", this.doneEdit);
		this.addBtn.addEventListener("click", this.addSlot);
		this.key = this.getAttribute("key");
		this.mode = "view";
		this.fixLabels();
	}

	clickEdit() {
		show(this.input);
		show(this.doneBtn);
		hide(this.editBtn);
		this.shadowRoot.querySelectorAll(".rem-btn").forEach((el) => {
			show(el);
		});
		this.nameInput.focus();
		this.mode = "edit";
	}

	doneEdit() {
		show(this.editBtn);
		hide(this.input);
		hide(this.doneBtn);
		this.shadowRoot.querySelectorAll(".rem-btn").forEach((el) => {
			hide(el);
		});
		this.mode = "view";
	}

	load(parent) {
		let val = parent[this.key];
		if (val != undefined) {
			this.value = val;
		} else {
			this.value = {};
		}
		this.calcWeight();
	}

	addSlot() {
		let id = uuidv4();
		let name = this.nameInput.value;
		if (!name) {
			return;
		}
		this.value[id] = {name, content: {}};
		this.updateList();
		this.dispatchEvent(
			new CustomEvent("change", {detail: [{key: this.key, value: this.value}]})
		);
	}
	remSlot(evt) {
		let id = evt.currentTarget.dataset.id;
		delete this.value[id];
		this.updateList();
		this.dispatchEvent(
			new CustomEvent("change", {detail: [{key: this.key, value: this.value}]})
		);
	}

	calcWeight() {
		let weight = 0;
		let keys = Object.keys(this.value).filter((key) =>
			this.value.hasOwnProperty(key)
		);
		keys.forEach((key) => {
			let slot = this.value[key];
			let items = Object.keys(slot.content).filter((key) =>
				slot.content.hasOwnProperty(key)
			);
			items.forEach((key) => {
				let item = slot.content[key];
				if (item.weight) {
					let itemWeight = parseFloat(item.weight);
					let itemQuantity = parseInt(item.quantity);
					if (!isNaN(itemWeight) && !isNaN(itemQuantity)) {
						weight += itemQuantity * itemWeight;
					}
				}
			});
		});
		this.shadowRoot.querySelector(".weight-output").textContent = `${
			Math.round(weight * 100) / 100
		} lbs`;
	}

	handleChange(evt) {
		// data is changed in place, so we only have to notify the change
		this.dispatchEvent(new CustomEvent("change", {detail: []}));
		// recalculate weight
		this.calcWeight();
	}

	updateList() {
		let keys = Object.keys(this.value).filter((key) =>
			this.value.hasOwnProperty(key)
		);
		// TODO only process relevant changes, leave elements that don't have to change
		clearContent(this.output);
		keys.forEach((key) => {
			let slot = this.value[key];
			let div = document.createElement("div");
			div.classList.add("mvxs");
			let el = document.createElement("cs-inv-slot-input");
			div.appendChild(el);
			el.value = slot;
			el.setAttribute("slot-id", key);
			el.addEventListener("change", this.handleChange);
			let a = document.createElement("button");
			addIcon(a, "bin", "remove");
			a.dataset.id = key;
			a.classList.add("rem-btn");
			a.classList.add("mvxs");
			if (this.mode == "view") {
				a.classList.add("hide");
			} else {
				a.classList.add("show");
			}
			a.addEventListener("click", this.remSlot);
			div.appendChild(a);
			this.output.appendChild(div);
		});
	}
}
customElements.define("cs-inventory", Inventory);
