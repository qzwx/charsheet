import {uuidv4} from "./util.js";

let currentChar = null;
let chars = null;

function load() {
	chars = localStorage.getItem("chars");
	if (!chars) {
		chars = {};
	} else {
		chars = JSON.parse(chars);
	}
	let currentId = localStorage.getItem("current");
	let char = localStorage.getItem(currentId);
	if (char) {
		currentChar = JSON.parse(char);
		decorate(currentChar);
	} else {
		newChar();
	}
}

function getLevel(filter) {
	return this.levels
		.map((l) => (!filter || l.class === filter ? l.level : 0))
		.reduce((a, b) => a + b, 0);
}

function getProficiencyBonus() {
	let lvl = this.getLevel();
	if (lvl < 5) {
		return 2;
	}
	if (lvl < 9) {
		return 3;
	}
	if (lvl < 13) {
		return 4;
	}
	if (lvl < 17) {
		return 5;
	}
	return 6;
}

function decorate(char) {
	char.getLevel = getLevel.bind(char);
	char.getProficiencyBonus = getProficiencyBonus.bind(char);
}

function newChar() {
	if (chars == null) {
		load();
	}
	currentChar = {
		uuid: uuidv4(),
		inventory: {},
		actions: {},
		spells: {}
	};
	chars[currentChar.uuid] = {
		uuid: currentChar.uuid,
		name: "Unknown"
	};
	localStorage.setItem("current", currentChar.uuid);
	localStorage.setItem("chars", JSON.stringify(chars));
	localStorage.setItem(currentChar.uuid, JSON.stringify(currentChar));
	decorate(currentChar);
	return currentChar;
}

function getChar() {
	if (chars == null) {
		load();
	}
	return currentChar;
}
function getCharList() {
	if (chars == null) {
		load();
	}
	return chars;
}

function storeChar() {
	if (chars == null) {
		load();
	}
	let charStr = JSON.stringify(currentChar);
	localStorage.setItem(currentChar.uuid, charStr);
}

function selectChar(uuid) {
	if (chars == null) {
		load();
	}
	let char = localStorage.getItem(uuid);
	if (char) {
		currentChar = JSON.parse(char);
		decorate(currentChar);
		localStorage.setItem("current", currentChar.uuid);
	}
}

function deleteChar(uuid) {
	if (chars == null) {
		load();
	}
	if (uuid == currentChar.uuid) {
		console.log("Can't delete");
	} else {
		localStorage.removeItem(uuid);
		delete chars[uuid];
		localStorage.setItem("chars", JSON.stringify(chars));
	}
}

async function readCharFromFile(f) {
	if (chars == null) {
		load();
	}
	return new Promise((resolve, reject) => {
		function readHandler(event) {
			currentChar = JSON.parse(event.target.result);
			if (!currentChar.uuid) {
				currentChar.uuid = uuidv4();
			}
			chars[currentChar.uuid] = {
				uuid: currentChar.uuid,
				name: currentChar.name ? currentChar.name : "Unknown"
			};
			localStorage.setItem("current", currentChar.uuid);
			localStorage.setItem("chars", JSON.stringify(chars));
			localStorage.setItem(currentChar.uuid, JSON.stringify(currentChar));
			decorate(currentChar);
			resolve(currentChar);
		}

		let reader = new FileReader();
		reader.addEventListener("load", readHandler);
		reader.readAsText(f);
	});
}

export default {
	deleteChar,
	getChar,
	getCharList,
	newChar,
	readCharFromFile,
	selectChar,
	storeChar
};
