import "./icon.js";

export default class Modal extends HTMLElement {
	constructor() {
		super();
		this.isVisible = false;
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
		<link rel="stylesheet" href="/css/style.css" type="text/css"/>
		<div class="modal">
			<div class="modal-content box">
				<div class="modal-header">
					<slot name="header"></slot>
					<span class="close"><cs-icon icon-name="close" alternative-text="close"></cs-icon></span>
				</div>
				<div class="modal-body">
					<slot><slot>
				</div>
			</div>
		</div>
		`;
		this.closeModal = this.closeModal.bind(this);
		this.openModal = this.openModal.bind(this);
	}
	connectedCallback() {
		this.shadowRoot
			.querySelector(".close")
			.addEventListener("click", this.closeModal.bind(this));
		this.modal = this.shadowRoot.querySelector(".modal");
	}
	openModal() {
		this.isVisible = true;
		this.modal.style.display = "block";
	}
	closeModal() {
		this.isVisible = false;
		this.modal.style.display = "none";
		this.dispatchEvent(new CustomEvent("modalclose"));
	}
}
customElements.define("cs-modal", Modal);
