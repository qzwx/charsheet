import "./actions.js";
import "./collapsible.js";
import "./counter.js";
import "./deathsaves-input.js";
import "./hitdice-input.js";
import "./icon.js";
import "./img-input.js";
import "./input.js";
import "./inventory.js";
import "./level-input.js";
import "./modal.js";
import "./resets.js";
import "./skill-input.js";
import "./spell-slots.js";
import "./spell-list.js";
import "./stat-input.js";
import "./textarea.js";
import character from "./character.js";

export default class CharacterSheet extends HTMLElement {
	constructor() {
		super();
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
		<link rel="stylesheet" href="/css/style.css" type="text/css"/>
		<div class="character-sheet">
			<div class="nobreak">
				<cs-collapsible label="Character" mode="open" print>
					<div class="row pv">
						<cs-img-input class="col char-input mhs" key="image"></cs-input>
					</div>
					<div class="row pv">
						<cs-input class="col char-input mhs" key="name" label="Name"></cs-input>
					</div>
					<div class="row pv stretch">
						<cs-input class="col char-input mhs" key="race" label="Race"></cs-input>
						<cs-input class="col char-input mhs" key="background" label="Background"></cs-input>
						<cs-input class="col char-input mhs" key="alignment" label="Alignment"></cs-input>
					</div>
					<div class="row pv stretch">
						<cs-level-input class="col char-input mhs" key="levels" label="Levels"></cs-level-input>
					</div>
					<div class="row pv stretch">
						<cs-input class="col char-input mhs" key="player" label="Player Name"></cs-input>
						<cs-input class="col char-input mhs" key="experience" label="Experience Points" type="number"></cs-input>
						<cs-input class="col char-input mhs" key="faction" label="Faction Name"></cs-input>
					</div>
					<div class="row pv stretch">
						<cs-input class="col char-input mhs" key="height" label="Height"></cs-input>
						<cs-input class="col char-input mhs" key="weight" label="Weight"></cs-input>
						<cs-input class="col char-input mhs" key="age" label="Age"></cs-input>
						<cs-input class="col char-input mhs" key="sex" label="Sex"></cs-input>
					</div>
					<div class="row pv stretch">
						<cs-input class="col char-input mhs" key="hair" label="Hair Color"></cs-input>
						<cs-input class="col char-input mhs" key="eyes" label="Eye Color"></cs-input>
						<cs-input class="col char-input mhs" key="skin" label="Skin Color"></cs-input>
					</div>
				</cs-collapsible>
			</div>
			<div class="nobreak">
				<cs-collapsible label="Description" mode="closed" toggle="desc" print>
					<cs-textarea class="char-input" key="traits" label="Personality Traits" rows="3"></cs-textarea>
					<cs-textarea class="char-input" key="ideals" label="Ideals" rows="3"></cs-textarea>
					<cs-textarea class="char-input" key="bonds" label="Bonds" rows="3"></cs-textarea>
					<cs-textarea class="char-input" key="flaws" label="Flaws" rows="3"></cs-textarea>
					<cs-textarea class="char-input" key="description" label="Description / Backstory" rows="10"></cs-textarea>
				</cs-collapsible>
			</div>
			<div class="nobreak">
				<cs-collapsible label="Stats" mode="open" toggle="stats" print>
					<div class="pt"><h3>Ability Scores</h3></div>
					<div class="row">
						<cs-stat-input class="col char-input mhs" key="str" label="Str"></cs-stat-input>
						<cs-stat-input class="col char-input mhs" key="dex" label="Dex"></cs-stat-input>
						<cs-stat-input class="col char-input mhs" key="con" label="Con"></cs-stat-input>
						<cs-stat-input class="col char-input mhs" key="int" label="Int"></cs-stat-input>
						<cs-stat-input class="col char-input mhs" key="wis" label="Wis"></cs-stat-input>
						<cs-stat-input class="col char-input mhs" key="cha" label="Cha"></cs-stat-input>
					</div>
					<div class="pt"><h3>Saves</h3></div>
					<div class="row">
						<cs-skill-input class="col char-input mhs" key="str-sav" label="Str" proficiency></cs-skill-input>
						<cs-skill-input class="col char-input mhs" key="dex-sav" label="Dex" proficiency></cs-skill-input>
						<cs-skill-input class="col char-input mhs" key="con-sav" label="Con" proficiency></cs-skill-input>
						<cs-skill-input class="col char-input mhs" key="int-sav" label="Int" proficiency></cs-skill-input>
						<cs-skill-input class="col char-input mhs" key="wis-sav" label="Wis" proficiency></cs-skill-input>
						<cs-skill-input class="col char-input mhs" key="cha-sav" label="Cha" proficiency></cs-skill-input>
					</div>
					<div class="pt"><h3>Skills</h3></div>
					<div class="row mv">
						<div class="col mhs">
							<cs-skill-input class="col char-input mvxs" mode="inline" key="acrobatics" label="Acrobatics (dex)" proficiency></cs-skill-input>
							<cs-skill-input class="col char-input mvxs" mode="inline" key="animal-handling" label="Animal Handling (wis)" proficiency></cs-skill-input>
							<cs-skill-input class="col char-input mvxs" mode="inline" key="arcana" label="Arcana (int)" proficiency></cs-skill-input>
							<cs-skill-input class="col char-input mvxs" mode="inline" key="athletics" label="Athletics (str)" proficiency></cs-skill-input>
							<cs-skill-input class="col char-input mvxs" mode="inline" key="deception" label="Deception (cha)" proficiency></cs-skill-input>
							<cs-skill-input class="col char-input mvxs" mode="inline" key="history" label="History (int)" proficiency></cs-skill-input>
							<cs-skill-input class="col char-input mvxs" mode="inline" key="insight" label="Insight (wis)" proficiency></cs-skill-input>
							<cs-skill-input class="col char-input mvxs" mode="inline" key="intimidation" label="Intimidation (cha)" proficiency></cs-skill-input>
							<cs-skill-input class="col char-input mvxs" mode="inline" key="investigation" label="Investigation (int)" proficiency></cs-skill-input>
							<cs-skill-input class="col char-input mvxs" mode="inline" key="medicine" label="Medicine (wis)" proficiency></cs-skill-input>
							<cs-skill-input class="col char-input mvxs" mode="inline" key="nature" label="Nature (int)" proficiency></cs-skill-input>
							<cs-skill-input class="col char-input mvxs" mode="inline" key="perception" label="Perception (wis)" proficiency></cs-skill-input>
							<cs-skill-input class="col char-input mvxs" mode="inline" key="performance" label="Performance (cha)" proficiency></cs-skill-input>
							<cs-skill-input class="col char-input mvxs" mode="inline" key="persuasion" label="Persuasion (cha)" proficiency></cs-skill-input>
							<cs-skill-input class="col char-input mvxs" mode="inline" key="religion" label="Religion (int)" proficiency></cs-skill-input>
							<cs-skill-input class="col char-input mvxs" mode="inline" key="sleight-of-hand" label="Sleight of Hand (dex)" proficiency></cs-skill-input>
							<cs-skill-input class="col char-input mvxs" mode="inline" key="stealth" label="Stealth (dex)" proficiency></cs-skill-input>
							<cs-skill-input class="col char-input mvxs" mode="inline" key="survival" label="Survival (wis)" proficiency></cs-skill-input>
						</div>
						<div class="col mhs">
							<cs-counter class="col char-input mvxs" key="hitpoints" label="Hitpoints" reset="long-rest"></cs-counter>
							<cs-counter class="col char-input mvxs" key="tmp-hitpoints" label="Temp Hitpoints" reset="long-rest"></cs-counter>
							<cs-hitdice-input class="col char-input mvxs" key="hitdice" label="Hit Dice"></cs-hitdice-input>
							<cs-death-saves-input class="col char-input mvxs" key="deathsaves" label="Death Saves"></cs-death-saves-input>
							<cs-skill-input class="col char-input mvxs" key="prof-bonus" label="Proficiency Bonus"></cs-skill-input>
							<cs-skill-input class="col char-input mvxs" key="armor-class" label="Armor Class"></cs-skill-input>
							<cs-skill-input class="col char-input mvxs" key="initiative" label="Initiative"></cs-skill-input>
							<cs-skill-input class="col char-input mvxs" key="speed" label="Speed"></cs-skill-input>
							<cs-skill-input class="col char-input mvxs" key="darkvision" label="Darkvision"></cs-skill-input>
							<cs-skill-input class="col char-input mvxs" key="attacks" label="Number of Attacks"></cs-skill-input>
							<cs-skill-input class="col char-input mvxs" key="passive-perception" label="Passive Perception"></cs-skill-input>
						</div>
					</div>
				</cs-collapsible>
			</div>
			<cs-collapsible label="Abilities" mode="closed" print>
				<cs-textarea class="char-input" key="abilities" label="Feats & Abilities" rows="10"></cs-textarea>
				<cs-textarea class="char-input" key="resistances" label="Resistances" rows="10"></cs-textarea>
			</cs-collapsible>
			<cs-collapsible label="Inventory" mode="closed" toggle="inventory" print>
				<cs-inventory class="char-input" key="inventory"></cs-inventory>
			</cs-collapsible>
			<cs-collapsible label="Actions" mode="closed" toggle="actions" print>
				<cs-actions class="char-input" key="actions"></cs-actions>
			</cs-collapsible>
			<cs-collapsible label="Magic" mode="closed" toggle="magic" print>
				<cs-spell-slots class="char-input" key="spell-slots"></cs-spell-slots>
				<cs-spell-list class="char-input" key="spell-list"></cs-spell-list>
			</cs-collapsible>
			<cs-collapsible label="Notes" mode="closed" toggle="notes">
				<cs-textarea class="char-input" key="notes"></cs-textarea>
			</cs-collapsible>
		</div>
		`;
		this.storeChar = this.storeChar.bind(this);
		this.reload = this.reload.bind(this);
	}
	connectedCallback() {
		this.shadowRoot.querySelectorAll(".char-input").forEach((el) => {
			el.addEventListener("change", this.storeChar);
		});
		if (document.location.host.startsWith("localhost:")) {
			let btn = document.createElement("button");
			btn.textContent = "Test";
			this.shadowRoot.appendChild(btn);
			btn.addEventListener("click", this.test);
		}
		this.reload();
	}

	storeChar(evt) {
		let char = character.getChar();
		evt.detail.forEach((change) => {
			if (change.key === "name") {
				character.getCharList()[char.uuid].name = change.value;
				this.dispatchEvent(new CustomEvent("change"));
			}
			char[change.key] = change.value;
		});
		character.storeChar();
	}

	reload() {
		let char = character.getChar();
		this.shadowRoot.querySelectorAll(".char-input").forEach((el) => {
			el.load(char);
		});
	}

	test() {
		let char = character.getChar();
		console.log("level", char.getLevel());
		console.log("prof bonus", char.getProficiencyBonus());
	}
}
customElements.define("cs-character-sheet", CharacterSheet);
