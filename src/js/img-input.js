import {show, hide} from "./util.js";

export default class ImgInput extends HTMLElement {
	_value = "";

	get value() {
		return this._value;
	}
	set value(val) {
		if (typeof val === "string") {
			val = val.trim();
		}
		this._value = val;
		if (this.input) {
			this.input.value = val;
		}
		if (this.output) {
			this.output.src = val;
			if (val) {
				show(this.output);
			} else {
				hide(this.output);
			}
		}
	}
	constructor() {
		super();
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
		<link rel="stylesheet" href="/css/style.css" type="text/css"/>
		<div class="cs-img-input">
			<div class="img-container">
				<img class="hide"/>
				<a class="edit-btn noprint"><cs-icon icon-name="pencil"  alternative-text="edit"></cs-icon></a><a class="done-btn noprint hide"><cs-icon icon-name="pin" alternative-text="done"></cs-icon></a>
			</div>
			<input class="input hide mv mh" type="text"></input>
		</div>
		`;
		this.clickEdit = this.clickEdit.bind(this);
		this.doneEdit = this.doneEdit.bind(this);
		this.load = this.load.bind(this);
	}
	connectedCallback() {
		this.input = this.shadowRoot.querySelector("input.input");
		this.output = this.shadowRoot.querySelector("img");
		this.editBtn = this.shadowRoot.querySelector("a.edit-btn");
		this.doneBtn = this.shadowRoot.querySelector("a.done-btn");
		this.editBtn.addEventListener("click", this.clickEdit);
		this.input.addEventListener("blur", this.doneEdit);
		this.key = this.getAttribute("key");
		let typ = this.getAttribute("type");
		if (typ) {
			this.input.type = typ;
		}
		this.input.value = this.value;
		this.output.textContent = this.value;
		this.mode = "output";
	}

	clickEdit() {
		this.mode = "input";
		show(this.input);
		show(this.doneBtn);
		hide(this.output);
		hide(this.editBtn);
		this.input.focus();
	}

	doneEdit() {
		this.value = this.input.value;
		this.output.textContent = this.value;
		this.mode = "output";
		show(this.output);
		show(this.editBtn);
		hide(this.input);
		hide(this.doneBtn);
		this.dispatchEvent(
			new CustomEvent("change", {detail: [{key: this.key, value: this.value}]})
		);
	}

	load(char) {
		let val = char[this.key];
		if (val != undefined) {
			this.value = val;
		} else {
			this.value = "";
		}
	}
}
customElements.define("cs-img-input", ImgInput);
