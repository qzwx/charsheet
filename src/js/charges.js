import showEditModal from "./counter-edit.js";
import {registerListener, deregisterListener} from "./event.js";

const DEDUPTIMEOUT = 500; // ms

export default class Charges extends HTMLElement {
	_value = 0;
	_comments = "";
	_used = 0;

	get value() {
		return this._value;
	}
	set value(val) {
		if (!val) {
			val = 0;
		}
		this._value = val;
		if (this.connected) {
			this.updateChecks();
		}
	}
	get used() {
		return this._used;
	}
	set used(val) {
		if (!val) {
			val = 0;
		}
		this._used = val;
		if (this.connected) {
			this.updateChecks();
		}
	}
	get comments() {
		return this._comments;
	}
	set comments(val) {
		if (!val) {
			val = "";
		}
		this._comments = val;
	}

	reset = "";

	constructor() {
		super();
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
		<link rel="stylesheet" href="/css/style.css" type="text/css"/>
		<div class="cs-charges">
			<div><label class="lbl"></label> <a class="edit-btn noprint"><cs-icon icon-name="pencil" alternative-text="edit"></cs-icon></a></div>
			<div class="output"></div>
		</div>
		`;
		this.clickEdit = this.clickEdit.bind(this);
		this.handleClick = this.handleClick.bind(this);
		this.load = this.load.bind(this);
		this.signalUsesChanged = this.signalUsesChanged.bind(this);
		this.updateChecks = this.updateChecks.bind(this);
		this.resetCallback = this.resetCallback.bind(this);
	}

	connectedCallback() {
		this.output = this.shadowRoot.querySelector("div.output");
		this.shadowRoot
			.querySelector("a.edit-btn")
			.addEventListener("click", this.clickEdit);
		let variant = this.getAttribute("variant");
		if (variant) {
			this.shadowRoot.querySelector(".cs-charges").classList.add(variant);
		}
		this.shadowRoot.querySelectorAll("label.lbl").forEach((itm) => {
			itm.textContent = this.getAttribute("label");
		});
		if (!this.key) {
			this.key = this.getAttribute("key");
		}
		this.connected = true;
		this.updateChecks();
		registerListener("reset", this.resetCallback);
	}

	disconnectedCallback() {
		deregisterListener("reset", this.resetCallback);
	}

	clickEdit() {
		showEditModal({
			value: this.value,
			comments: this.comments,
			reset: this.reset
		})
			.then((values) => {
				if (values) {
					this.value = values.value;
					this.comments = values.comments;
					this.reset = values.reset;
					this.updateChecks();
					let detail = [
						{key: this.key, value: this.value},
						{key: this.key + "-reset", value: this.reset},
						{key: this.key + "-comments", value: this.comments}
					];
					this.dispatchEvent(
						new CustomEvent("change", {
							detail: detail
						})
					);
				}
			})
			.catch((err) => {
				console.error(err);
				// TODO toast?
			});
	}

	resetCallback(payload) {
		if (
			this.reset &&
			(payload.type === this.reset ||
				(payload.type === "long-rest" && this.reset === "short-rest"))
		) {
			this.used = 0;
			this.signalUsesChanged();
		}
	}

	load(parent) {
		let val = parent[this.key];
		if (val) {
			this.value = val;
		} else {
			this.value = 0;
		}
		let com = parent[this.key + "-comments"];
		if (com) {
			this.comments = com;
		} else {
			this.comments = "";
		}
		let res = parent[this.key + "-reset"];
		if (res !== undefined) {
			this.reset = res;
		} else {
			this.reset = this.getAttribute("reset") || "";
		}
		let used = parent[this.key + "-used"];
		if (used) {
			this.used = used;
		} else {
			this.used = 0;
		}
	}

	handleClick() {
		clearTimeout(this.timer);
		let used = this.checks
			.map((cb) => (cb.checked ? 1 : 0))
			.reduce((p, c) => p + c, 0);
		this.used = used;
		this.timer = setTimeout(this.signalUsesChanged, DEDUPTIMEOUT);
	}

	signalUsesChanged() {
		this.dispatchEvent(
			new CustomEvent("change", {
				detail: [{key: this.key + "-used", value: this.used}]
			})
		);
	}

	updateChecks() {
		this.checks = [...this.shadowRoot.querySelectorAll("input.charge")];
		this.checks.forEach((el, idx) => {
			if (idx >= this.value) {
				el.parentNode.removeChild(el);
			} else {
				el.checked = this.used > idx;
			}
		});
		for (let idx = this.checks.length; idx < this.value; idx++) {
			let el = document.createElement("input");
			el.type = "checkbox";
			el.classList.add("charge");
			el.checked = this.used > idx;
			el.addEventListener("click", this.handleClick);
			this.output.appendChild(el);
			this.checks.push(el);
		}
	}
}
customElements.define("cs-charges", Charges);
