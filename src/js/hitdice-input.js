import {show, hide, clearContent} from "./util.js";
import {registerListener, deregisterListener} from "./event.js";

const DEDUPTIMEOUT = 500; // ms

export default class HitDiceInput extends HTMLElement {
	_value = {};

	get value() {
		return this._value;
	}
	set value(val) {
		if (typeof val === "string") {
			val = val.trim();
		}
		this._value = val;
		this.updateList();
	}
	constructor() {
		super();
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
		<link rel="stylesheet" href="/css/style.css" type="text/css"/>
		<div class="cs-input">
			<div><label></label> <a class="edit-btn noprint"><cs-icon icon-name="pencil"  alternative-text="edit"></cs-icon></a><a class="done-btn noprint hide"><cs-icon icon-name="pin" alternative-text="done"></cs-icon></a></div>
			<div class="output mb"></div>
			<div class="input noprint hide">
				<div class="row mb">
					<div class="mh col">
						<select class="die-select">
							<option>d4</option>
							<option>d6</option>
							<option>d8</option>
							<option>d10</option>
							<option>d12</option>
						</select>
					</div>
					<div class="mh col">
						<button class="add-btn">Add</button>
					</div>
					<div class="mh col">
						<button class="rem-btn">Remove</button>
					</div>
				</div>
			</div>
		</div>
		`;
		this.addDie = this.addDie.bind(this);
		this.clickEdit = this.clickEdit.bind(this);
		this.doneEdit = this.doneEdit.bind(this);
		this.handleClick = this.handleClick.bind(this);
		this.load = this.load.bind(this);
		this.remDie = this.remDie.bind(this);
		this.resetCallback = this.resetCallback.bind(this);
		this.signalChanged = this.signalChanged.bind(this);
		this.updateList = this.updateList.bind(this);
	}
	connectedCallback() {
		this.input = this.shadowRoot.querySelector("div.input");
		this.output = this.shadowRoot.querySelector("div.output");
		this.editBtn = this.shadowRoot.querySelector("a.edit-btn");
		this.doneBtn = this.shadowRoot.querySelector("a.done-btn");
		this.addBtn = this.shadowRoot.querySelector("button.add-btn");
		this.remBtn = this.shadowRoot.querySelector("button.rem-btn");
		this.dieSel = this.shadowRoot.querySelector("select.die-select");
		this.editBtn.addEventListener("click", this.clickEdit);
		this.doneBtn.addEventListener("click", this.doneEdit);
		this.addBtn.addEventListener("click", this.addDie);
		this.remBtn.addEventListener("click", this.remDie);
		this.shadowRoot.querySelector("label").textContent = this.getAttribute(
			"label"
		);
		this.key = this.getAttribute("key");
		let typ = this.getAttribute("type");
		if (typ) {
			this.input.type = typ;
		}
		this.updateList();
		registerListener("reset", this.resetCallback);
	}

	disconnectedCallback() {
		deregisterListener("reset", this.resetCallback);
	}

	clickEdit() {
		show(this.input);
		show(this.doneBtn);
		hide(this.editBtn);
		this.dieSel.focus();
	}

	doneEdit() {
		hide(this.input);
		hide(this.doneBtn);
		show(this.editBtn);
		this.dispatchEvent(
			new CustomEvent("change", {
				detail: [{key: this.key, value: this.value}]
			})
		);
	}

	handleClick(evt) {
		clearTimeout(this.timer);
		let die = evt.currentTarget.dataset.die;
		let checks = [
			...this.shadowRoot.querySelectorAll(
				`input[type="checkbox"][data-die="${die}"]`
			)
		];
		let used = checks
			.map((cb) => (cb.checked ? 1 : 0))
			.reduce((p, c) => p + c, 0);
		checks.forEach((cb, idx) => {
			cb.checked = used > idx;
		});
		this.value[die].used = used;
		this.timer = setTimeout(this.signalChanged, DEDUPTIMEOUT);
	}

	updateList() {
		let dice = Object.keys(this.value)
			.filter((key) => this.value.hasOwnProperty(key))
			.map((key) => this.value[key])
			.filter((die) => die.total > 0);
		// TODO only process relevant changes, leave elements that don't have to change
		clearContent(this.output);
		dice.forEach((die, idx) => {
			let dieDiv = document.createElement("div");
			dieDiv.classList.add("box");
			dieDiv.classList.add("mh");
			dieDiv.classList.add("mvxs");
			dieDiv.classList.add("row");
			let dieSpan = document.createElement("span");
			dieDiv.appendChild(dieSpan);
			dieSpan.classList.add("col");
			dieSpan.textContent = `${die.total}${die.die}`;
			let checkSpan = document.createElement("span");
			dieDiv.appendChild(checkSpan);
			for (let i = 0; i < die.total; ++i) {
				let check = document.createElement("input");
				checkSpan.appendChild(check);
				check.type = "checkbox";
				check.classList.add(die.die);
				check.dataset.die = die.die;
				check.checked = die.used > i;
				check.addEventListener("click", this.handleClick);
			}
			this.output.appendChild(dieDiv);
		});
	}

	signalChanged() {
		this.dispatchEvent(
			new CustomEvent("change", {
				detail: [{key: this.key, value: this.value}]
			})
		);
	}

	load(parent) {
		let val = parent[this.key];
		if (val != undefined) {
			this.value = val;
		} else {
			this.value = {};
		}
	}

	addDie() {
		let die = this.dieSel.value;
		if (!this.value[die]) {
			this.value[die] = {die, total: 1, used: 0};
		} else {
			this.value[die].total += 1;
		}
		this.updateList();
	}

	remDie() {
		let die = this.dieSel.value;
		if (this.value[die] && this.value[die].total > 0) {
			this.value[die].total -= 1;
		}
		this.updateList();
	}

	resetCallback(payload) {
		if (payload.type === "long-rest") {
			Object.keys(this.value).forEach((die) => {
				if (this.value.hasOwnProperty(die) && this.value[die].used != 0) {
					this.value[die].used = Math.max(
						this.value[die].used - Math.ceil(this.value[die].total / 2),
						0
					);
				}
			});
			this.updateList();
			this.signalChanged();
		}
	}
}
customElements.define("cs-hitdice-input", HitDiceInput);
