import "./modal.js";
import {MODIFIERS} from "./util.js";
import {merge} from "./parser.js";
import {registerListener, deregisterListener} from "./event.js";

const INVALID_SCORE = "#ERR#";
// prettier-ignore

export default class StatInput extends HTMLElement {
	_value = "";

	get value() {
		return this._value;
	}
	set value(val) {
		if (typeof val === "string") {
			val = val.trim();
		}
		this._value = val;
		if (this.input) {
			this.input.value = val;
		}
		if (this.output) {
			this.output.textContent = merge(val, this.key);
		}
	}
	get comments() {
		return this._comments;
	}
	set comments(val) {
		this._comments = val;
		if (this.commentsInput) {
			this.commentsInput.value = val;
		}
	}
	constructor() {
		super();
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
		<link rel="stylesheet" href="/css/style.css" type="text/css"/>
		<div class="cs-stat-input">
			<div><label></label> <a class="edit-btn noprint"><cs-icon icon-name="pencil" alternative-text="edit"></cs-icon></a></div>
			<div class="output"></div>
			<div class="mod-output"></div>
		</div>
		<cs-modal class="edit-modal">
			<label slot="header"></label>
			<div><input class="input"></input></div>
			<div><textarea class="comments-input" rows="10"></textarea></div>
		</cs-modal>
		`;
		this.clickEdit = this.clickEdit.bind(this);
		this.doneEdit = this.doneEdit.bind(this);
		this.load = this.load.bind(this);
		this.setModOutput = this.setModOutput.bind(this);
		this.recalcCallback = this.recalcCallback.bind(this);
	}
	connectedCallback() {
		this.input = this.shadowRoot.querySelector("input.input");
		this.commentsInput = this.shadowRoot.querySelector(
			"textarea.comments-input"
		);
		this.output = this.shadowRoot.querySelector("div.output");
		this.modOutput = this.shadowRoot.querySelector("div.mod-output");
		this.editBtn = this.shadowRoot.querySelector("a.edit-btn");
		this.editBtn.addEventListener("click", this.clickEdit);
		this.modal = this.shadowRoot.querySelector("cs-modal.edit-modal");
		this.modal.addEventListener("modalclose", this.doneEdit);
		this.shadowRoot.querySelectorAll("label").forEach((itm) => {
			itm.textContent = this.getAttribute("label");
		});
		this.key = this.getAttribute("key");
		this.input.value = this.value;
		this.output.textContent = merge(this.value, this.key);
		this.mode = "output";
		registerListener("recalc", this.recalcCallback);
	}

	disconnectedCallback() {
		deregisterListener("recalc", this.recalcCallback);
	}

	recalcCallback(){
		this.output.textContent = merge(this.value, this.key);
	}

	clickEdit() {
		this.mode = "input";
		this.modal.openModal();
		this.input.focus();
	}

	doneEdit() {
		this.value = this.input.value;
		this.comments = this.commentsInput.value;
		this.output.textContent = this.value;
		this.setModOutput();
		this.mode = "output";
		this.dispatchEvent(
			new CustomEvent("change", {
				detail: [
					{key: this.key, value: this.value},
					{key: this.key + "-comments", value: this.comments}
				]
			})
		);
	}

	setModOutput() {
		let val = parseInt(merge(this.value, this.key));
		if (this.value === undefined || this.value === null || this.value === "") {
			this.modOutput.textContent = "";
		} else if (isNaN(val) || val <= 0) {
			this.modOutput.textContent = INVALID_SCORE;
		} else {
			let m = MODIFIERS[Math.min(val, 30)];
			this.modOutput.textContent = (m >= 0 ? `+${m}` : `${m}`);
		}
	}

	load(parent) {
		let val = parent[this.key];
		if (val !== undefined) {
			this.value = val;
		} else {
			this.value = "";
		}
		let com = parent[this.key + "-comments"];
		if (com !== undefined) {
			this.comments = com;
		} else {
			this.comments = "";
		}
		this.setModOutput();
	}
}
customElements.define("cs-stat-input", StatInput);
