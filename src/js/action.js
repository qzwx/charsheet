import "./counter.js";
import "./charges.js";
import {clearContent} from "./util.js";
import {merge} from "./parser.js";
import {registerListener, deregisterListener} from "./event.js";

export default class Action extends HTMLElement {
	_value = {};

	get value() {
		return this._value;
	}
	set value(val) {
		this._value = val;
	}
	constructor() {
		super();
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
		<link rel="stylesheet" href="/css/style.css" type="text/css"/>
		<div class="cs-action">
			<div class="row spread">
				<div class="output"></div>
				<a class="edit-btn noprint"><cs-icon icon-name="pencil" alternative-text="edit"></cs-icon></a>
			</div>
		</div>
		`;
		this.clickEdit = this.clickEdit.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.updateOutput = this.updateOutput.bind(this);
		this.recalcCallback = this.recalcCallback.bind(this);
	}
	connectedCallback() {
		this.output = this.shadowRoot.querySelector("div.output");
		this.shadowRoot
			.querySelector("a.edit-btn")
			.addEventListener("click", this.clickEdit);
		this.key = this.getAttribute("key");
		this.updateOutput();
		this.connected = true;
		registerListener("recalc", this.recalcCallback);
	}

	disconnectedCallback() {
		deregisterListener("recalc", this.recalcCallback);
	}

	recalcCallback() {
		this.updateOutput();
	}

	clickEdit() {
		this.dispatchEvent(new CustomEvent("edit"));
	}

	handleChange(evt) {
		evt.detail.forEach((change) => {
			this.value[change.key] = change.value;
		});
		this.dispatchEvent(
			new CustomEvent("change", {detail: [{key: this.key, value: this.value}]})
		);
	}

	updateOutput() {
		clearContent(this.output);
		if (this.value.type === "Counter") {
			let counter = document.createElement("cs-counter");
			counter.setAttribute("variant", "action");
			counter.setAttribute("label", merge(this.value.name, this.key));
			counter.key = "counter";
			counter.load(this.value);
			counter.addEventListener("change", this.handleChange);
			this.output.appendChild(counter);
		} else if (this.value.type === "Charges") {
			let charges = document.createElement("cs-charges");
			charges.setAttribute("variant", "action");
			charges.setAttribute("label", merge(this.value.name, this.key));
			charges.key = "charges";
			charges.load(this.value);
			charges.addEventListener("change", this.handleChange);
			this.output.appendChild(charges);
		} else {
			let nameSpan = document.createElement("span");
			nameSpan.textContent = merge(this.value.name, this.key);
			this.output.appendChild(nameSpan);
		}
	}
}
customElements.define("cs-action", Action);
