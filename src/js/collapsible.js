import {show, hide} from "./util.js";
import {registerListener, deregisterListener} from "./event.js";

export default class Collapsible extends HTMLElement {
	get mode() {
		return this._mode;
	}
	set mode(value) {
		this._mode = value;
		if (this.container) {
			this.updateClasses();
		}
	}
	constructor() {
		super();
		this.isVisible = false;
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
		<link rel="stylesheet" href="/css/style.css" type="text/css"/>
		<div class="collapsible box">
			<div>
				<a class="close-btn noprint hide"><cs-icon icon-name="book-open" alternative-text="close"></cs-icon></a>
				<a class="open-btn noprint"><cs-icon icon-name="book" alternative-text="open"></cs-icon></a>
				<slot name="header"></slot>
			</div>
			<div class="container">
				<slot></slot>
			</div>
		</div>
		`;
		this.close = this.close.bind(this);
		this.open = this.open.bind(this);
		this.toggle = this.toggle.bind(this);
		this.updateClasses = this.updateClasses.bind(this);
	}

	attributeChangedCallback(name, oldVal, newVal) {
		if (name === "mode") {
			this.mode = newVal;
		}
	}

	connectedCallback() {
		this.container = this.shadowRoot.querySelector("div.container");
		this.openBtn = this.shadowRoot.querySelector("a.open-btn");
		this.openBtn.addEventListener("click", () => {
			this.mode = "open";
		});
		this.closeBtn = this.shadowRoot.querySelector("a.close-btn");
		this.closeBtn.addEventListener("click", () => {
			this.mode = "closed";
		});
		this.mode = this.getAttribute("mode");
		let label = this.getAttribute("label");
		if (label) {
			let head = document.createElement("h3");
			head.slot = "header";
			head.textContent = label;
			this.appendChild(head);
		}
		if (this.getAttribute("print") != null) {
			this.container.classList.add("print");
		}
		registerListener("open-all", this.open);
		registerListener("close-all", this.close);
		this.toggleEvent = this.getAttribute("toggle");
		if (this.toggleEvent) {
			registerListener(`toggle-${this.toggleEvent}`, this.toggle);
		}
	}

	disconnectedCallback() {
		deregisterListener("open-all", this.open);
		deregisterListener("close-all", this.close);
		if (this.toggleEvent) {
			deregisterListener(`toggle-${this.toggleEvent}`, this.toggle);
		}
	}

	updateClasses() {
		if (this.mode === "closed") {
			hide(this.container);
			hide(this.closeBtn);
			show(this.openBtn);
		} else {
			show(this.container);
			show(this.closeBtn);
			hide(this.openBtn);
		}
	}

	open() {
		this.mode = "open";
	}
	close() {
		this.mode = "closed";
	}
	toggle(payload) {
		if (payload.shift) {
			this.close();
		} else {
			this.open();
			this.scrollIntoView();
		}
	}
}
customElements.define("cs-collapsible", Collapsible);
