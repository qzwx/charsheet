import {show, hide} from "./util.js";

const DEDUPTIMEOUT = 500; // ms

export default class DeathSavesInput extends HTMLElement {
	_value = {failed: 0, passed: 0};

	get value() {
		return this._value;
	}
	set value(val) {
		this._value = val;
		this.updateChecks();
	}
	constructor() {
		super();
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
		<link rel="stylesheet" href="/css/style.css" type="text/css"/>
		<div class="cs-input">
			<div><label class="lbl"></label></div>
			<div class="row">
				<label class="col">Passed</label>
				<span class="col">
					<input class="input passed" type="checkbox"></input>
					<input class="input passed" type="checkbox"></input>
					<input class="input passed" type="checkbox"></input>
				</span>
			</div>
			<div class="row">
				<label class="col">Failed</label>
				<span class="col">
					<input class="input failed" type="checkbox"></input>
					<input class="input failed" type="checkbox"></input>
					<input class="input failed" type="checkbox"></input>
				</span>
			</div>
		</div>
		`;
		this.handleChange = this.handleChange.bind(this);
		this.load = this.load.bind(this);
		this.signalChanged = this.signalChanged.bind(this);
		this.updateChecks = this.updateChecks.bind(this);
	}
	connectedCallback() {
		this.failed = [...this.shadowRoot.querySelectorAll(".failed")];
		this.passed = [...this.shadowRoot.querySelectorAll(".passed")];
		this.failed.forEach((cb) => {
			cb.addEventListener("click", this.handleChange);
		});
		this.passed.forEach((cb) => {
			cb.addEventListener("click", this.handleChange);
		});
		this.shadowRoot.querySelector("label.lbl").textContent = this.getAttribute(
			"label"
		);
		this.key = this.getAttribute("key");
	}

	load(char) {
		let val = char[this.key];
		if (val != undefined) {
			this.value = val;
		} else {
			this.value = {failed: 0, passed: 0};
		}
	}

	handleChange() {
		clearTimeout(this.timer);
		let passed = this.passed
			.map((cb) => (cb.checked ? 1 : 0))
			.reduce((p, c) => p + c, 0);
		let failed = this.failed
			.map((cb) => (cb.checked ? 1 : 0))
			.reduce((p, c) => p + c, 0);
		this.value = {
			passed,
			failed
		};
		this.timer = setTimeout(this.signalChanged, DEDUPTIMEOUT);
	}

	signalChanged() {
		this.dispatchEvent(
			new CustomEvent("change", {
				detail: [{key: this.key, value: this.value}]
			})
		);
	}

	updateChecks() {
		this.failed.forEach((cb, idx) => {
			cb.checked = this.value.failed > idx;
		});
		this.passed.forEach((cb, idx) => {
			cb.checked = this.value.passed > idx;
		});
	}
}
customElements.define("cs-death-saves-input", DeathSavesInput);
