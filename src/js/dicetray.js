import character from "./character.js";
import {parse} from "./parser.js";

export default class DiceTray extends HTMLElement {
	constructor() {
		super();
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
		<link rel="stylesheet" href="/css/style.css" type="text/css"/>
		<div>
			<div class="cs-scrollable">
				<ul class="rolls">
				</ul>
			</div>
			<div class="row pv">
				<input class="roll-input" type="text" value="1d20"></input>
				<span class="ph">
					<button class="btn roll-btn">🎲 Roll</button>
				</span>
			</div>
			<div class="row">
				<span class="pr"><button class="btn dice-btn" data-d="4">d4</button></span>
				<span class="pr"><button class="btn dice-btn" data-d="6">d6</button></span>
				<span class="pr"><button class="btn dice-btn" data-d="8">d8</button></span>
				<span class="pr"><button class="btn dice-btn" data-d="10">d10</button></span>
				<span class="pr"><button class="btn dice-btn" data-d="12">d12</button></span>
				<span class="pr"><button class="btn dice-btn" data-d="20">d20</button></span>
				<span class="pr"><button class="btn dice-btn" data-d="100">d100</button></span>
			</div>
		</div>
		`;
		this.roll = this.roll.bind(this);
		this.addDice = this.addDice.bind(this);
		this.reroll = this.reroll.bind(this);
	}
	connectedCallback() {
		this.input = this.shadowRoot.querySelector("input.roll-input");
		this.rolls = this.shadowRoot.querySelector("ul.rolls");
		this.shadowRoot
			.querySelector("button.roll-btn")
			.addEventListener("click", this.roll);
		this.shadowRoot
			.querySelectorAll("button.dice-btn")
			.forEach((btn) => btn.addEventListener("click", this.addDice));
	}

	roll() {
		try {
			let input = this.input.value;
			if (!input) {
				return;
			}
			let ast = parse(input);
			let char = character.getChar();
			char.rolls = [];
			let result = ast.execute(char);
			let li = document.createElement("li");
			li.dataset.expr = input;
			let inputRow = document.createElement("div");
			inputRow.textContent = input;
			li.appendChild(inputRow);
			let rollsMap = new Map();
			char.rolls.forEach((roll) => {
				if (rollsMap.has(roll.die)) {
					rollsMap.get(roll.die).push(roll.value);
				} else {
					rollsMap.set(roll.die, [roll.value]);
				}
			});
			[...rollsMap.keys()].forEach((key) => {
				let values = rollsMap.get(key);
				let rollsDiv = document.createElement("div");
				rollsDiv.textContent = `d${key} → ${values.join(", ")}`;
				li.appendChild(rollsDiv);
			});
			let outputRow = document.createElement("div");
			outputRow.textContent = `= ${result}`;
			li.appendChild(outputRow);
			this.rolls.appendChild(li);
			li.scrollIntoView(false);
			li.addEventListener("click", this.reroll);
			this.input.value = "";
		} catch (err) {
			console.error(err);
		}
	}

	addDice(evt) {
		try {
			let d = evt.target.dataset.d;
			console.log("addDice", d);
			let input = this.input.value;
			if (!input) {
				input = "";
			}
			let p = input.split(/\b/);
			let lastIdx = p.length - 1;
			let m = p[lastIdx].match(new RegExp(`(\\d+)d${d}`));
			if (m) {
				p[lastIdx] = parseInt(m[1]) + 1 + "d" + d;
			} else {
				if (input.length > 1) {
					p.push(" + ");
				}
				p.push("1d" + d);
			}
			this.input.value = p.join("");
		} catch (err) {
			console.error(err);
		}
	}

	reroll(evt) {
		this.input.value = evt.currentTarget.dataset.expr;
	}
}
customElements.define("cs-dicetray", DiceTray);
