import {parse} from "./parser.js";

let n = 1;
let success = true;

function log(...args) {
	let p = document.createElement("p");
	let s = args.map((a) => {
		if (typeof a === "string") {
			return a;
		} else if (typeof a === "number") {
			return a.toString();
		} else if (a === undefined) {
			return "undefined";
		} else if (a === null) {
			return "undefined";
		} else {
			return JSON.stringify(a);
		}
	});
	p.textContent = s.join(", ");
	let div = document.getElementById("results");
	div.appendChild(p);
}
function assert(expr, msg) {
	if (!expr) {
		let p = document.createElement("p");
		p.style.color = "red";
		p.textContent = msg;
		let div = document.getElementById("results");
		div.appendChild(p);
	}
}

function assertEq(exp, act) {
	if (exp !== act) {
		success = false;
	}
	assert(exp === act, `Error, expected\n${exp}, got\n${act}`);
}
function testParse(input, repr) {
	log(`TEST ${n++}`, input, repr);

	let result = parse(input);
	assertEq(repr, result.repr);
}
function testParseErr(input, err) {
	log(`TEST ${n++}`, input, err);
	let errMsg = null;
	try {
		parse(input); // should throw error;
	} catch (err) {
		errMsg = err.message;
	}
	assertEq(err, errMsg);
}
function testExec(input, ctx, output) {
	log(`TEST ${n++}`, input, ctx, output);
	let ast = parse(input);
	//log(ast.repr);
	assertEq(output, ast.execute(ctx));
}

function testDie(input, eMin, eMax, eAvg) {
	let nIt = 1000000;
	log(`TEST ${n++} DIE`, input);
	let dMin = eMax;
	let dMax = eMin;
	let rolls = [];
	let ast = parse(input);
	for (let i = 0; i < nIt; ++i) {
		let n = ast.execute({});
		rolls.push(n);
		dMin = Math.min(dMin, n);
		dMax = Math.max(dMax, n);
	}
	assertEq(eMin, dMin);
	assertEq(eMax, dMax);
	let dAvg = rolls.reduce((a, b) => a + b, 0) / rolls.length;
	log(`${nIt} rolls, min: ${dMin}, max: ${dMax}, average: ${dAvg}`);
	assert(
		Math.abs(dAvg - eAvg) < 0.05,
		`Error, expected\n${eAvg}, got\n${dAvg}`
	);
}

testParse("1", "1");
testParse("1 + 2 * 3", "(+ 1 (* 2 3))");
testParse("a + b * c * d + e", "(+ (+ a (* (* b c) d)) e)");
testParse("--1 * 2", "(* (- (- 1)) 2)");
testParse("--(1 * 2)", "(- (- (* 1 2)))");
testParse("-(1)", "(- 1)");
testParse("min(1, 2)", "(min 1 2)");
testParse("min(1, 2) * max(3, 4, 2 + 3)", "(* (min 1 2) (max 3 4 (+ 2 3)))");
testParseErr(
	"foo, bar",
	`Parsing ended before consuming all input. Terminated at position 3. Unconsumed: ", bar"`
);

testExec("1", {}, 1);
testExec("1 + 2 * 3", {}, 7);
testExec("--(1 * 2)", {}, 2);
testExec("-(5 - 2)", {}, -3);
testExec("a + b * c * d + e", {a: 1, b: 3, c: 5, d: 7, e: 2}, 108);
testExec("min(3, 5)", {}, 3);
testExec("max(3 * 3, 5)", {}, 9);
testExec("max(pow(2, 2), sqrt(25))", {}, 5);
testExec("pow(floor(x / y), max(2, 3))", {x: 5, y: 2}, 8);
testDie("1d4", 1, 4, 2.5);
testDie("1d6", 1, 6, 3.5);
testDie("1d10", 1, 10, 5.5);
testDie("1d20", 1, 20, 10.5);
testDie("3d6", 3, 18, 10.5);
testDie("5d4", 5, 20, 12.5);
if (success) {
	log("All tests successful");
}
