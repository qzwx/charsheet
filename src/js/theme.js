import {show, hide} from "./util.js";

class ThemeSetter extends HTMLElement {
	constructor() {
		super();
		this.isVisible = false;
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
		<link rel="stylesheet" href="/css/style.css" type="text/css"/>
		<div class="popover-container noprint" style="position:fixed; top: 0; right: .25em; padding: .25em;">
			<div class="settings-btn btn" style="text-align: right"><cs-icon icon-name="star" alternative-text="Select theme"><cs-icon></div>
			<div class="popover-content hide">
				<div class="btn theme-btn" data-theme="forest-night">Forest Night</div>
				<div class="btn theme-btn" data-theme="phoenix-fire">Phoenix Fire</div>
				<div class="btn theme-btn" data-theme="dwarven-mine">Dwarven Mine</div>
				<div class="btn theme-btn" data-theme="unicorn-poo">Unicorn Poo</div>
				<div class="btn theme-btn" data-theme="high-contrast">High Contrast</div>
				<div class="btn theme-btn" data-theme="high-contrast-inverse">High Contrast (inversed)</div>
			</div>
		</div>
		`;
		this.setTheme = this.setTheme.bind(this);
		this.toggleSettings = this.toggleSettings.bind(this);
	}
	connectedCallback() {
		this.settingsBtn = this.shadowRoot.querySelector(".settings-btn");
		this.popover = this.shadowRoot.querySelector(".popover-content");
		this.settingsBtn.addEventListener("click", this.toggleSettings);
		this.shadowRoot.querySelectorAll(".theme-btn").forEach((btn) => {
			btn.addEventListener("click", this.setTheme);
		});
	}
	toggleSettings(evt) {
		show(this.popover);
	}
	setTheme(evt) {
		let theme = evt.currentTarget.dataset.theme;
		document.body.className = theme;
		localStorage.setItem("theme", theme);
		hide(this.popover);
	}
}
customElements.define("cs-theme-setter", ThemeSetter);

window.addEventListener("load", function init() {
	let theme = localStorage.getItem("theme");
	if (theme) {
		document.body.className = theme;
	}
	document.body.appendChild(document.createElement("cs-theme-setter"));
});
