import "./icon.js";
import "./modal.js";
import {merge} from "./parser.js";
import {show, hide, uuidv4} from "./util.js";
import {registerListener, deregisterListener} from "./event.js";

export default class SkillInput extends HTMLElement {
	_value = "";

	get value() {
		return this._value;
	}
	set value(val) {
		if (typeof val === "string") {
			val = val.trim();
		}
		this._value = val;
		if (this.input) {
			this.input.value = val;
		}
		if (this.output) {
			this.output.textContent = merge(val, this.key);
		}
	}
	get comments() {
		return this._comments;
	}
	set comments(val) {
		this._comments = val;
		if (this.commentsInput) {
			this.commentsInput.value = val;
		}
	}

	get proficiency() {
		return this._proficiency;
	}
	set proficiency(val) {
		this._proficiency = val;
		if (this.profOutput) {
			if (val) {
				show(this.profOutput);
			} else {
				hide(this.profOutput);
			}
		}
		if (this.profInput) {
			this.profInput.checked = val;
		}
	}

	constructor() {
		super();
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
		<link rel="stylesheet" href="/css/style.css" type="text/css"/>
		<div class="cs-skill-input">
			<div><label class="lbl"></label> <a class="edit-btn noprint"><cs-icon icon-name="pencil" alternative-text="edit"></cs-icon></a></div>
			<cs-icon class="prof-output hide" icon-name="fire"></cs-icon><div class="output"></div>
		</div>
		<cs-modal class="edit-modal">
			<label slot="header" class="lbl"></label>
			<div><input class="input" type="text"></input></div>
			<div class="prof hide pv"><input class="prof-input input" data-field="prof" type="checkbox"></input><label data-for="prof">Proficiency</label></div>
			<div><textarea class="comments-input" rows="10"></textarea></div>
		</cs-modal>
		`;
		this.clickEdit = this.clickEdit.bind(this);
		this.doneEdit = this.doneEdit.bind(this);
		this.load = this.load.bind(this);
		this.recalcCallback = this.recalcCallback.bind(this);
	}
	connectedCallback() {
		this.input = this.shadowRoot.querySelector("input.input");
		this.commentsInput = this.shadowRoot.querySelector(
			"textarea.comments-input"
		);
		this.profInput = this.shadowRoot.querySelector("input.prof-input");
		this.output = this.shadowRoot.querySelector("div.output");
		this.profOutput = this.shadowRoot.querySelector("cs-icon.prof-output");
		this.editBtn = this.shadowRoot.querySelector("a.edit-btn");
		this.editBtn.addEventListener("click", this.clickEdit);
		this.modal = this.shadowRoot.querySelector("cs-modal.edit-modal");
		this.modal.addEventListener("modalclose", this.doneEdit);
		this.shadowRoot.querySelectorAll("label.lbl").forEach((itm) => {
			itm.textContent = this.getAttribute("label");
		});
		this.key = this.getAttribute("key");
		let proficiency = this.getAttribute("proficiency");
		this.enableProficiency = proficiency !== null;
		if (this.enableProficiency) {
			let profDiv = this.shadowRoot.querySelector("div.prof");
			show(profDiv);
		}
		if (this.getAttribute("mode") === "inline") {
			let div = this.shadowRoot.querySelector(".cs-skill-input");
			div.classList.add("row");
			div.classList.add("spread");
			div.classList.add("ph");
			div.classList.add("center-items");
		}
		this.input.value = this.value;
		this.output.textContent = merge(this.value, this.key);
		this.mode = "output";
		this.fixLabels();
		registerListener("recalc", this.recalcCallback);
	}

	disconnectedCallback() {
		deregisterListener("recalc", this.recalcCallback);
	}

	recalcCallback() {
		this.output.textContent = merge(this.value, this.key);
	}

	fixLabels() {
		let inputs = new Map();
		this.shadowRoot.querySelectorAll(".input").forEach((el) => {
			if (el.dataset.field) {
				inputs.set(el.dataset.field, el);
			}
		});
		this.shadowRoot.querySelectorAll("label").forEach((el) => {
			let input = inputs.get(el.dataset.for);
			if (input) {
				let id = uuidv4();
				input.setAttribute("id", id);
				el.setAttribute("for", id);
			}
		});
	}

	clickEdit() {
		this.mode = "input";
		this.modal.openModal();
		this.input.focus();
	}

	doneEdit() {
		this.value = this.input.value;
		this.comments = this.commentsInput.value;
		this.output.textContent = merge(this.value, this.key);
		this.mode = "output";
		let detail = [
			{key: this.key, value: this.value},
			{key: this.key + "-comments", value: this.comments}
		];
		if (this.enableProficiency) {
			this.proficiency = this.profInput.checked;
			detail.push({key: this.key + "-proficiency", value: this.proficiency});
		}
		this.dispatchEvent(
			new CustomEvent("change", {
				detail: detail
			})
		);
	}

	load(parent) {
		let val = parent[this.key];
		if (val !== undefined) {
			this.value = val;
		} else {
			this.value = "";
		}
		let com = parent[this.key + "-comments"];
		if (com !== undefined) {
			this.comments = com;
		} else {
			this.comments = "";
		}
		if (this.enableProficiency) {
			let prof = parent[this.key + "-proficiency"];
			this.proficiency = !!prof;
		}
	}
}
customElements.define("cs-skill-input", SkillInput);
