import "./collapsible.js";
import showEditModal from "./spell-edit.js";
import {addIcon, clearContent, uuidv4, show, hide} from "./util.js";
import {merge} from "./parser.js";
import {registerListener, deregisterListener} from "./event.js";

export default class SpellList extends HTMLElement {
	_value = {};

	get value() {
		return this._value;
	}
	set value(val) {
		this._value = val;
		if (this.connected) {
			this.updateList();
		}
	}
	constructor() {
		super();
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
		<link rel="stylesheet" href="/css/style.css" type="text/css"/>
		<div class="cs-spell-slots">
			<div class="row spread"><label>Spell List</label></div>
			<div class="spells-list"></div>
			<div class="noprint">
				<div class="row">
					<button class="mh mv col toggle-prep-btn">Prepared only</button>
					<button class="mh mv col add-btn">Add</button>
				</div>
			</div>
		</div>
		`;
		this.addSpell = this.addSpell.bind(this);
		this.clickEdit = this.clickEdit.bind(this);
		this.load = this.load.bind(this);
		this.setSpellVisibility = this.setSpellVisibility.bind(this);
		this.togglePrep = this.togglePrep.bind(this);
		this.updateList = this.updateList.bind(this);
		this.recalcCallback = this.recalcCallback.bind(this);
	}
	connectedCallback() {
		this.spellsList = this.shadowRoot.querySelector("div.spells-list");
		this.shadowRoot
			.querySelector("button.add-btn")
			.addEventListener("click", this.addSpell);
		this.togglePrepBtn = this.shadowRoot.querySelector(
			"button.toggle-prep-btn"
		);
		this.togglePrepBtn.addEventListener("click", this.togglePrep);
		this.key = this.getAttribute("key");
		this.connected = true;
		this.updateList();
		this.prepOnly = false;
		registerListener("recalc", this.recalcCallback);
	}

	disconnectedCallback() {
		deregisterListener("recalc", this.recalcCallback);
	}

	recalcCallback() {
		this.updateList();
	}
	clickEdit(evt) {
		let values = {};
		if (evt) {
			values.id = evt.currentTarget.dataset.id;
			values.mode = "edit";
			values.spell = this.value[values.id];
		} else {
			values.id = uuidv4();
			values.mode = "new";
			values.spell = {};
		}
		showEditModal(values)
			.then((values) => {
				if (values) {
					if (values.action === "save") {
						this.value[values.id] = values.spell;
					} else if (values.action === "delete") {
						delete this.value[values.id];
					}
					this.updateList();
					this.dispatchEvent(
						new CustomEvent("change", {
							detail: [{key: this.key, value: this.value}]
						})
					);
				}
			})
			.catch((err) => {
				console.error(err);
				// TODO toast?
			});
	}

	load(char) {
		let val = char[this.key];
		if (val != undefined) {
			this.value = val;
		} else {
			this.value = {};
		}
	}

	addSpell() {
		this.clickEdit(null);
	}

	addEl(parent, type, text, className) {
		let el = document.createElement(type);
		if (className) {
			el.className = className;
		}
		if (text) {
			el.textContent = merge(text, "spells");
		}
		parent.appendChild(el);
	}

	togglePrep() {
		this.prepOnly = !this.prepOnly;
		this.togglePrepBtn.textContent = this.prepOnly
			? "Show All"
			: "Prepared Only";
		this.shadowRoot
			.querySelectorAll("cs-collapsible.spell")
			.forEach(this.setSpellVisibility);
	}

	setSpellVisibility(el) {
		if (!this.prepOnly || el.dataset.prepared === "true") {
			show(el);
		} else {
			hide(el);
		}
	}

	updateList() {
		let keys = Object.keys(this.value).filter((key) =>
			this.value.hasOwnProperty(key)
		);
		keys.sort((a, b) => {
			let v1 = this.value[a];
			let v2 = this.value[b];
			if (v1.level != v2.level) {
				return v1.level - v2.level;
			}
			return v1.name.localeCompare(v2.name);
		});
		// TODO only process relevant changes, leave elements that don't have to change
		clearContent(this.spellsList);
		keys.forEach((key) => {
			let spell = this.value[key];
			let div = document.createElement("div");
			div.classList.add("nobreak");
			let coll = document.createElement("cs-collapsible");
			coll.classList.add("mv");
			coll.classList.add("pv");
			coll.classList.add("spell");
			coll.dataset.prepared = spell.prepared;
			coll.setAttribute("mode", "closed");
			coll.setAttribute("print", "");
			//coll.setAttribute("toggle", "magic");
			let summary = document.createElement("span");
			summary.slot = "header";
			summary.classList.add("row");
			summary.classList.add("spread");
			summary.classList.add("mvxs");
			summary.classList.add("ml");
			summary.style.width = "calc(100% - 2rem)";
			this.addEl(summary, "span", spell.name, "spell-name pr col30");
			this.addEl(summary, "span", "LVL: " + spell.level, "spell-lvl ph col");
			this.addEl(summary, "span", spell.range, "spell-range ph col");
			this.addEl(summary, "span", spell.time, "spell-time ph col");
			this.addEl(summary, "span", "DC: " + spell.dc, "spell-dc ph col");
			// TODO icons
			let checks = document.createElement("span");
			checks.classList.add("col");
			this.addEl(checks, "span", spell.ritual ? "R" : "", "spell-ritual");
			this.addEl(
				checks,
				"span",
				spell.concentration ? "C" : "",
				"spell-concentration"
			);
			this.addEl(checks, "span", spell.prepared ? "P" : "", "spell-prepared");
			summary.appendChild(checks);
			let editBtn = document.createElement("a");
			addIcon(editBtn, "pencil", "edit", "noprint");
			editBtn.className = "text-right";
			editBtn.dataset.id = key;
			editBtn.addEventListener("click", this.clickEdit);
			summary.appendChild(editBtn);
			coll.appendChild(summary);
			let descDiv = document.createElement("div");
			coll.appendChild(descDiv);
			this.addEl(descDiv, "div", "School: " + spell.school, "spell-school");
			this.addEl(
				descDiv,
				"div",
				"Components: " + spell.components,
				"spell-comp"
			);
			this.addEl(
				descDiv,
				"div",
				"Duration: " + spell.duration,
				"spell-duration"
			);
			this.addEl(descDiv, "br");
			this.addEl(descDiv, "div", spell.description, "spell-description pre");
			this.setSpellVisibility(coll);
			div.appendChild(coll);
			this.spellsList.appendChild(div);
		});
	}
}
customElements.define("cs-spell-list", SpellList);
