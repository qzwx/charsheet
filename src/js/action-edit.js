import "./modal.js";
import {show, hide, fixLabels, getElValue, setElValue, nvl} from "./util.js";

class ActionEdit extends HTMLElement {
	constructor() {
		super();
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
<link rel="stylesheet" href="/css/style.css" type="text/css"/>
<cs-modal>
	<label class="modal-label" slot="header">Action Edit</label>
	<div class="row mvxs center-items"><label class="col pr" data-for="name">Name</label><input class="col name-input" type="text" data-field="name"></input></div>
	<div class="row mvxs center-items">
		<label class="col pr" data-for="type">Type</label>
		<select class="col type-input" data-type="type">
			<option>Default</option>
			<option>Counter</option>
			<option>Charges</option>
		</select>
	</div>
	<div><label data-for="description">Description</label></div>
	<div><textarea class="description-input" data-field="description" rows="10"></textarea></div>
	<div class="row mv">
		<button class="delete-btn col mhxs">Delete</button>
		<button class="cancel-btn col mhxs">Cancel</button>
		<button class="save-btn col mhxs">Save</button>
	</div>
</cs-modal>
		`;
		this.fixLabels = fixLabels.bind(this);
		this.getElValue = getElValue.bind(this);
		this.handleCancel = this.handleCancel.bind(this);
		this.handleDelete = this.handleDelete.bind(this);
		this.handleSave = this.handleSave.bind(this);
		this.openModal = this.openModal.bind(this);
		this.setElValue = setElValue.bind(this);
	}
	connectedCallback() {
		this.modal = this.shadowRoot.querySelector("cs-modal");
		this.modal.addEventListener("close", this.handleCancel);
		this.modalLabel = this.shadowRoot.querySelector(".modal-label");
		this.shadowRoot
			.querySelector(".delete-btn")
			.addEventListener("click", this.handleDelete);
		this.shadowRoot
			.querySelector(".cancel-btn")
			.addEventListener("click", this.handleCancel);
		this.shadowRoot
			.querySelector(".save-btn")
			.addEventListener("click", this.handleSave);
		this.fixLabels();
	}

	handleError(err) {
		console.error(err);
		document.body.removeChild(this);
		this.reject(err);
	}
	init(values, resolve, reject) {
		this.resolve = resolve;
		this.reject = reject;
		try {
			this.itemId = values.id;
			if (values.action === "edit") {
				show(this.shadowRoot.querySelector(".delete-btn"));
				this.modalLabel.textContent = "Edit Action";
			} else {
				// new
				hide(this.shadowRoot.querySelector(".delete-btn"));
				this.modalLabel.textContent = "Add Action";
			}
			this.setElValue(".name-input", nvl(values.item.name, ""));
			this.setElValue(".type-input", nvl(values.item.type, 1));
			this.setElValue(".description-input", nvl(values.item.description, ""));
		} catch (err) {
			this.handleError(err);
		}
	}
	handleSave() {
		try {
			const rv = {
				id: this.itemId,
				action: "save",
				item: {
					name: this.getElValue(".name-input"),
					type: this.getElValue(".type-input"),
					description: this.getElValue(".description-input")
				}
			};
			document.body.removeChild(this);
			this.resolve(rv);
		} catch (err) {
			this.handleError(err);
		}
	}
	handleCancel() {
		document.body.removeChild(this);
		this.resolve(null);
	}
	handleDelete() {
		document.body.removeChild(this);
		this.resolve({
			id: this.itemId,
			action: "delete"
		});
	}
	openModal() {
		this.modal.openModal();
		this.shadowRoot.querySelector("input.name-input").focus();
	}
}
customElements.define("cs-action-edit", ActionEdit);

function showEditModal(values) {
	return new Promise((resolve, reject) => {
		let editModal = document.createElement("cs-action-edit");
		document.body.appendChild(editModal);
		editModal.init(values, resolve, reject);
		editModal.openModal();
	});
}

export default showEditModal;
