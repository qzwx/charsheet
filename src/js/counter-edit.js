import "./modal.js";
import {fixLabels, getElValue, setElValue} from "./util.js";

class CounterEdit extends HTMLElement {
	constructor() {
		super();
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
<link rel="stylesheet" href="/css/style.css" type="text/css"/>
<cs-modal>
	<label slot="header" class="lbl"></label>
	<div class="row mvxs center-items"><label class="col" data-for="value">Value</label><input class="col mh input" data-field="value" type="text"></input></div>
	<div class="row mvxs center-items">
		<label class="col" data-for="reset">Reset</label>
		<select class="col mh input reset-input" data-field="reset">
			<option value="">None</option>
			<option value="short-rest">Short Rest</option>
			<option value="long-rest">Long Rest</option>
			<option value="dusk">Dusk</option>
			<option value="dawn">Dawn</option>
		</select>
	</div>
	<div><label data-for="comments">Comments</label></div>
	<div><textarea class="input comments-input" data-field="comments" rows="10"></textarea></div>
	<div class="row mt">
		<button class="cancel-btn col mhxs">Cancel</button>
		<button class="save-btn col mhxs">Save</button>
	</div>
</cs-modal>
		`;
		this.fixLabels = fixLabels.bind(this);
		this.getElValue = getElValue.bind(this);
		this.handleCancel = this.handleCancel.bind(this);
		this.handleSave = this.handleSave.bind(this);
		this.openModal = this.openModal.bind(this);
		this.setElValue = setElValue.bind(this);
	}
	connectedCallback() {
		this.modal = this.shadowRoot.querySelector("cs-modal");
		this.modal.addEventListener("close", this.handleCancel);
		this.shadowRoot
			.querySelector(".cancel-btn")
			.addEventListener("click", this.handleCancel);
		this.shadowRoot
			.querySelector(".save-btn")
			.addEventListener("click", this.handleSave);
		this.fixLabels();
	}

	handleError(err) {
		console.error(err);
		document.body.removeChild(this);
		this.reject(err);
	}
	init(values, resolve, reject) {
		this.resolve = resolve;
		this.reject = reject;
		try {
			this.setElValue(".input", values.value);
			this.setElValue(".reset-input", values.reset);
			this.setElValue(".comments-input", values.comments);
		} catch (err) {
			this.handleError(err);
		}
	}
	handleSave() {
		try {
			const rv = {
				value: this.getElValue(".input"),
				comments: this.getElValue(".comments-input"),
				reset: this.getElValue(".reset-input")
			};
			document.body.removeChild(this);
			this.resolve(rv);
		} catch (err) {
			this.handleError(err);
		}
	}
	handleCancel() {
		document.body.removeChild(this);
		this.resolve(null);
	}
	openModal() {
		this.modal.openModal();
	}
}
customElements.define("cs-counter-edit", CounterEdit);

function showEditModal(values) {
	return new Promise((resolve, reject) => {
		let editModal = document.createElement("cs-counter-edit");
		document.body.appendChild(editModal);
		editModal.init(values, resolve, reject);
		editModal.openModal();
	});
}

export default showEditModal;
