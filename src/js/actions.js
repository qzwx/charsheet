import "./action.js";
import showEditModal from "./action-edit.js";
import {uuidv4, clearContent} from "./util.js";
import {merge} from "./parser.js";

export default class Actions extends HTMLElement {
	_value = {};

	get value() {
		return this._value;
	}
	set value(val) {
		this._value = val;
		this.updateList();
	}
	constructor() {
		super();
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
		<link rel="stylesheet" href="/css/style.css" type="text/css"/>
		<div class="cs-actions">
			<div class="btn-container"></div>
			<div class="action-list"></div>
			<div class="noprint mv">
				<button class="add-btn">Add Action</button>
			</div>
		</div>
		`;
		this.addAction = this.addAction.bind(this);
		this.clickEdit = this.clickEdit.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.load = this.load.bind(this);
		this.updateList = this.updateList.bind(this);
	}
	connectedCallback() {
		this.input = this.shadowRoot.querySelector("div.input");
		this.actionList = this.shadowRoot.querySelector("div.action-list");
		this.addBtn = this.shadowRoot.querySelector("button.add-btn");
		this.addBtn.addEventListener("click", this.addAction);
		this.key = this.getAttribute("key");
		this.mode = "view";
	}

	clickEdit(evt) {
		let values = {};
		if (evt) {
			values.id = evt.target.key;
			values.action = "edit";
			values.item = this.value[values.id];
		} else {
			values.id = uuidv4();
			values.action = "new";
			values.item = {};
		}
		showEditModal(values)
			.then((values) => {
				if (values) {
					if (values.action === "save") {
						this.value[values.id] = values.item;
					} else if (values.action === "delete") {
						delete this.value[values.id];
					}
					this.updateList();
					// data is changed in place, so we only have to notify the change
					this.dispatchEvent(
						new CustomEvent("change", {
							detail: []
						})
					);
				}
			})
			.catch((err) => {
				console.error(err);
				// TODO toast?
			});
	}

	load(char) {
		let val = char[this.key];
		if (val != undefined) {
			this.value = val;
		} else {
			this.value = {};
		}
	}

	addAction() {
		this.clickEdit(null);
	}

	handleChange() {
		// data is changed in place, so we only have to notify the change
		this.dispatchEvent(
			new CustomEvent("change", {
				detail: []
			})
		);
	}

	updateList() {
		let keys = Object.keys(this.value).filter((key) =>
			this.value.hasOwnProperty(key)
		);
		// TODO only process relevant changes, leave elements that don't have to change
		clearContent(this.actionList);
		keys.forEach((key) => {
			let action = this.value[key];
			let div = document.createElement("div");
			div.classList.add("row");
			div.classList.add("spread");
			div.classList.add("mvxs");
			let el = document.createElement("cs-action");
			div.appendChild(el);
			el.value = action;
			el.setAttribute("key", key);
			el.dataset.id = key;
			el.addEventListener("change", this.handleChange);
			el.addEventListener("edit", this.clickEdit);
			this.actionList.appendChild(div);
		});
	}
}
customElements.define("cs-actions", Actions);
