import showEditModal from "./counter-edit.js";
import {registerListener, deregisterListener} from "./event.js";
import {merge} from "./parser.js";

const FILTEREXP = /[^ 0-9+=-]/g;
const CALCEXP = /\s*([+=-]?)\s*(\d+)/g;
const DEDUPTIMEOUT = 500; // ms

export default class Counter extends HTMLElement {
	_value = "";
	reset = null;

	get value() {
		return this._value;
	}
	set value(val) {
		this._value = val;
		if (this.output) {
			this.output.textContent = merge(val, this.key);
		}
	}
	get comments() {
		return this._comments;
	}
	set comments(val) {
		this._comments = val;
	}

	get current() {
		return this._current;
	}
	set current(val) {
		this._current = val;
		if (this.currentOutput) {
			this.currentOutput.textContent = val;
		}
	}

	get calc() {
		return this._calc;
	}
	set calc(val) {
		if (typeof val == "string") {
			val = val.replace(FILTEREXP, "");
		}
		this._calc = val;
		if (this.calcInput) {
			this.calcInput.value = val;
		}
		this.calculate(val);
	}

	constructor() {
		super();
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
		<link rel="stylesheet" href="/css/style.css" type="text/css"/>
		<div class="cs-counter">
			<div><label class="lbl"></label><a class="edit-btn noprint"><cs-icon icon-name="pencil"  alternative-text="edit"></cs-icon></a></div>
			<div><span class="current-output"></span><span> / </span><span class="output"></span></div>
			<div class="mb mh noprint"><input class="calc-input" type="text"></input></div>
		</div>
		`;
		this.calculate = this.calculate.bind(this);
		this.clickEdit = this.clickEdit.bind(this);
		this.handleCalcBlur = this.handleCalcBlur.bind(this);
		this.handleCalcInput = this.handleCalcInput.bind(this);
		this.load = this.load.bind(this);
		this.signalCalcChanged = this.signalCalcChanged.bind(this);
		this.resetCallback = this.resetCallback.bind(this);
		this.recalcCallback = this.recalcCallback.bind(this);
	}
	connectedCallback() {
		this.calcInput = this.shadowRoot.querySelector("input.calc-input");
		this.output = this.shadowRoot.querySelector("span.output");
		this.currentOutput = this.shadowRoot.querySelector("span.current-output");
		this.editBtn = this.shadowRoot.querySelector("a.edit-btn");
		this.editBtn.addEventListener("click", this.clickEdit);
		this.shadowRoot.querySelectorAll("label.lbl").forEach((itm) => {
			itm.textContent = this.getAttribute("label");
		});
		let variant = this.getAttribute("variant");
		if (variant) {
			this.shadowRoot.querySelector(".cs-counter").classList.add(variant);
		}
		if (!this.key) {
			this.key = this.getAttribute("key");
		}
		let reset = this.getAttribute("reset");
		if (reset && !this.reset) {
			this.reset = reset;
		}
		this.output.textContent = merge(this.value, this.key);
		this.mode = "output";
		this.calcInput.addEventListener("input", this.handleCalcInput);
		this.calcInput.addEventListener("blur", this.handleCalcBlur);
		if (this.calc) {
			this.calcInput.value = this.calc;
		}
		this.calculate(this.calc);
		registerListener("reset", this.resetCallback);
		registerListener("recalc", this.recalcCallback);
	}

	disconnectedCallback() {
		deregisterListener("reset", this.resetCallback);
		deregisterListener("recalc", this.recalcCallback);
	}

	recalcCallback() {
		this.output.textContent = merge(this.value, this.key);
		this.calculate(this.calc);
	}

	clickEdit() {
		showEditModal({
			value: this.value,
			comments: this.comments,
			reset: this.reset
		})
			.then((values) => {
				console.log(this.key, "edit", values);
				if (values) {
					this.value = values.value;
					this.comments = values.comments;
					this.reset = values.reset;
					this.calculate(this.calc);
					let detail = [
						{key: this.key, value: this.value},
						{key: this.key + "-reset", value: this.reset},
						{key: this.key + "-comments", value: this.comments}
					];
					this.dispatchEvent(
						new CustomEvent("change", {
							detail: detail
						})
					);
				}
			})
			.catch((err) => {
				console.error(err);
				// TODO toast?
			});
	}

	resetCallback(payload) {
		if (
			this.reset &&
			(payload.type === this.reset ||
				(payload.type === "long-rest" && this.reset === "short-rest"))
		) {
			this.calc = "";
			this.signalCalcChanged();
		}
	}

	load(parent) {
		let val = parent[this.key];
		if (val !== undefined) {
			this.value = val;
		} else {
			this.value = "0";
		}
		let com = parent[this.key + "-comments"];
		if (com !== undefined) {
			this.comments = com;
		} else {
			this.comments = "";
		}
		let res = parent[this.key + "-reset"];
		if (res !== undefined) {
			this.reset = res;
		} else {
			this.reset = this.getAttribute("reset") || "";
		}
		let calc = parent[this.key + "-calc"];
		if (calc !== undefined) {
			this.calc = calc;
		} else {
			this.calc = "";
		}
		this.calculate(this.calc);
	}

	handleCalcInput(evt) {
		clearTimeout(this.timer);
		this.calc = evt.currentTarget.value;
		this.timer = setTimeout(this.signalCalcChanged, DEDUPTIMEOUT);
	}

	handleCalcBlur(evt) {
		clearTimeout(this.timer);
		this.calc = evt.currentTarget.value;
		this.signalCalcChanged();
	}

	signalCalcChanged() {
		this.dispatchEvent(
			new CustomEvent("change", {
				detail: [{key: this.key + "-calc", value: this.calc}]
			})
		);
	}

	calculate(inp) {
		if (!inp) {
			inp = "";
		}
		let it = inp.matchAll(CALCEXP);
		let hp = parseInt(merge(this.value, this.key));
		if (isNaN(hp)) {
			hp = 0;
		}
		let m = it.next();
		while (!m.done) {
			let n = parseInt(m.value[2]);
			switch (m.value[1]) {
				case "+":
					hp += n;
					break;
				case "-":
					hp -= n;
					break;
				case "=":
				case "":
					hp = n;
					break;
			}
			m = it.next();
		}
		this.current = hp;
	}
}
customElements.define("cs-counter", Counter);
