import {addIcon, uuidv4, clearContent} from "./util.js";
import showEditModal from "./inventory-edit.js";

export default class InventorySlotInput extends HTMLElement {
	_value = "";

	get value() {
		return this._value;
	}
	set value(val) {
		this._value = val;
		if (this.connected) {
			this.label.textContent = val.name;
			this.updateList();
		}
	}
	constructor() {
		super();
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
		<link rel="stylesheet" href="/css/style.css" type="text/css"/>
		<div class="cs-inv-slot-input box">
			<div><label class="name"></label></div>
			<div class="weight-output secondary-text"></div>
			<div class="items-list"></div>
			<div class="mv mhs">
				<button class="add-btn noprint">Add Item</button>
			</div>
		</div>
		`;
		this.clickEdit = this.clickEdit.bind(this);
		this.clickAdd = this.clickAdd.bind(this);
		this.updateList = this.updateList.bind(this);
	}
	connectedCallback() {
		this.addBtn = this.shadowRoot.querySelector("button.add-btn");
		this.addBtn.addEventListener("click", this.clickAdd);
		this.itemsList = this.shadowRoot.querySelector("div.items-list");
		this.label = this.shadowRoot.querySelector("label.name");
		this.key = this.getAttribute("key");
		this.slotId = this.getAttribute("slot-id");
		if (this.value) {
			this.label.textContent = this.value.name ? this.value.name : "Unknown";
		}
		this.updateList();
		this.connected = true;
	}

	clickEdit(evt) {
		let values = {};
		if (evt) {
			values.id = evt.currentTarget.parentNode.dataset.itemId;
			values.action = "edit";
			values.item = this.value.content[values.id];
		} else {
			values.id = uuidv4();
			values.action = "new";
			values.item = {};
		}
		showEditModal(values)
			.then((values) => {
				if (values) {
					if (values.action === "save") {
						this.value.content[values.id] = values.item;
					} else if (values.action === "delete") {
						delete this.value.content[values.id];
					}
					this.updateList();
					this.dispatchEvent(
						new CustomEvent("change", {
							detail: [{key: this.key, value: this.value}]
						})
					);
				}
			})
			.catch((err) => {
				console.error(err);
				// TODO toast?
			});
	}

	clickAdd() {
		this.clickEdit(null);
	}

	updateList() {
		let items = Object.keys(this.value.content).filter((key) =>
			this.value.content.hasOwnProperty(key)
		);
		// TODO only process relevant changes, leave elements that don't have to change
		// TODO split over two columns?
		clearContent(this.itemsList);
		let weight = 0;
		items.forEach((key) => {
			let item = this.value.content[key];
			let itemDiv = document.createElement("div");
			itemDiv.dataset.itemId = key;
			itemDiv.classList.add("row");
			itemDiv.classList.add("box");
			itemDiv.classList.add("mvxs");
			let nameSpan = document.createElement("span");
			nameSpan.classList.add("col");
			nameSpan.classList.add("name");
			nameSpan.textContent = item.name;
			itemDiv.appendChild(nameSpan);
			let quantitySpan = document.createElement("span");
			quantitySpan.classList.add("col");
			quantitySpan.classList.add("quantity");
			quantitySpan.classList.add("text-right");
			quantitySpan.classList.add("ph");
			quantitySpan.textContent = item.quantity;
			itemDiv.appendChild(quantitySpan);
			let editBtn = document.createElement("a");
			addIcon(editBtn, "pencil", "edit", "noprint");
			editBtn.addEventListener("click", this.clickEdit);
			editBtn.classList.add("text-right");
			itemDiv.appendChild(editBtn);
			this.itemsList.appendChild(itemDiv);
			if (item.weight) {
				let itemWeight = parseFloat(item.weight);
				let itemQuantity = parseInt(item.quantity);
				if (!isNaN(itemWeight) && !isNaN(itemQuantity)) {
					weight += itemQuantity * itemWeight;
				}
			}
		});
		this.shadowRoot.querySelector(".weight-output").textContent = `${
			Math.round(weight * 100) / 100
		} lbs`;
	}
}
customElements.define("cs-inv-slot-input", InventorySlotInput);
