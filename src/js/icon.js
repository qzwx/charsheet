// remove ]+;
const icons = {
	pencil: `
<path
	style="stroke-width:0.5291667;stroke-linecap:butt;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
	d="M 6.2838538,0.81264886 C 7.406876,-0.2688698 8.8217461,1.1701917 7.8902528,2.2300595 L 3.3309152,6.7421503 C 3.3456043,5.5517538 2.4867895,5.3707379 1.8898808,5.3719865 Z"
	id="path824"/>
<path
	style="stroke-width:0.5291667;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
	d="M 2.8820684,5.4901039 6.9453124,1.4504837"
	id="path828"/>
<path
	style="stroke-width:0.5291667;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
	d="M 1.8898808,5.3719864 0.59058779,7.9233258 3.3309152,6.7421503"
	id="path830"/>
<path
	style="stroke-width:0.52916667;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"
	d="M 6.2838538,0.81264886 7.8902527,2.2300595"
	id="path832" />
`,
	pin: `
<path
	style="stroke-width:0.5291667;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
	d="M 2.2014981,3.6599838 5.1458777,6.5972375 5.0914389,5.051081 7.1147114,2.524727 7.9804518,2.3870456 6.2358903,0.54696741 5.9906772,1.5489779 3.8694405,3.5687724 Z"
	id="path834"/>
<path
	style="stroke-width:0.5291667;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
	d="M 3.1700656,4.8333656 0.23360773,8.2533448 3.8079633,5.4179663 Z"
	id="path836"/>
`,
	bin: `
<path
	style="stroke-width:0.52916667;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"
	d="M 1.5827753,3.033259 2.763951,8.1123138 5.5042781,8.0886905 6.6145832,3.1513764"
	id="path857" />
<ellipse
	style="fill-opacity:0;stroke-width:0.53348649;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
	id="path859"
	cx="4.0986791"
	cy="3.1868117"
	rx="2.4901206"
	ry="1.0727098" />
<path
	style="stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	d="M 2.2206101,3.4348586 2.6930802,2.7970239 3.000186,3.7655878 3.4726562,2.9623885 4.2286086,2.5607887 4.4175965,3.1041295 5.0081843,3.6710937 5.3861607,2.9151413 6.0003718,3.1986235"
	id="path863" />
<path
	style="stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	d="M 3.7561382,3.5293527 4.4175965,3.1041295 5.5515253,3.5057291"
	id="path865" />
`,
	book: `
<path
	style="stroke-width:0.5291667;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
	d="M 0.85989586,6.7185267 H 6.7185262 V 0.62366085 H 0.85989586 Z"
	id="path1486" />
 <path
	style="stroke-width:0.5291667;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
	d="M 1.5213541,7.7106403 C 1.1328089,7.5479949 0.87140191,7.2582113 0.85989586,6.7185267"
	id="path1461"/>
 <path
	style="stroke-width:0.5291667;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
	d="m 6.7185262,1.1528275 0.661385,0.9922612"
	id="path1463" />
 <path
	style="stroke-width:0.5291667;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
	d="M 1.5213541,7.7106401 H 7.379985 L 7.3799112,1.615922"
	id="path1465"/>
 <rect
	style="opacity:1;fill-opacity:0;stroke-width:0.52916667;stroke-linecap:round;stroke-linejoin:bevel;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1;paint-order:markers fill stroke"
	id="rect1488"
	width="3.1655507"
	height="1.1575521"
	x="2.1733632"
	y="1.8284599" />
`,
	"book-open": `
 <path
	style="stroke-width:0.5291667;stroke-linecap:square;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1;paint-order:normal"
	d="M 0.89746798,7.1805141 C 2.1423835,7.2419541 2.988033,6.4373603 4.346501,7.4639966 5.175796,6.5670808 6.576259,7.2912786 7.630169,7.1805136 L 7.606549,1.1328951 C 6.405765,1.0960859 5.248772,0.27544055 4.346505,1.0147776 3.265909,0.07448015 2.066969,1.3091364 0.87384847,1.1565186 Z"
	id="path1498-0-3"/>
 <path
	style="stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	d="m 4.3465048,1.0147776 -3.9e-6,6.4492189"
	id="path1544" />
 <path
	style="stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	d="M 1.4410342,2.4993676 C 2.2914807,2.4237669 3.1419271,1.8165521 3.9923736,2.3103795"
	id="path1546"/>
 <path
	style="stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	d="M 1.4372995,4.3807952 C 2.3596423,4.3160039 3.2234709,3.9047658 3.7760272,4.1681835"
	id="path1548-9"/>
 <path
	style="stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	d="M 1.3900525,6.0580645 C 2.3123953,5.9932732 3.1762239,5.5820352 3.7287802,5.8454528"
	id="path1548-5"/>
 <path
	style="stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	d="M 6.9925595,2.6316591 C 6.3711046,2.6092976 5.2841013,2.2464269 4.8191963,2.5844122"
	id="path1580"/>
 <path
	style="stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	d="M 4.971177,3.9090259 C 5.3834009,3.5974111 6.5046392,3.9801186 7.1209165,4.0271435"
	id="path1578-4"/>
 <path
	style="stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	d="M 7.050046,5.9784453 C 6.4285911,5.9560838 5.3415878,5.5932131 4.8766828,5.9311984"
	id="path1580-9"/>
 <path
	style="stroke-width:0.266884;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
	d="M 1.643038,1.0972076 H 0.30810835 V 7.7330397 H 8.1962817 V 1.0972076 H 7.2260815"
	id="path1661" />
`,
	fire: `
<path
	style="fill:#ff8800ff;stroke-width:0.5291667;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:0"
	d="M 1.4174106,5.7735862 C 2.4349377,9.571049 6.3322818,8.3714911 7.110677,5.4664805 7.4060521,4.2497885 7.0534746,3.2085788 6.5437127,2.4190477 6.1892878,5.7748405 5.4742328,4.7445017 5.0318079,3.9781994 4.6347818,3.0179698 4.8580705,1.9794709 5.1971726,0.93076643 4.1392429,1.8823662 3.6004565,3.092071 3.236421,4.4506696 2.6443146,4.1135846 2.1593941,3.6220667 2.1969865,2.6552828 1.6497822,3.2024871 0.97536782,4.1746574 1.4174106,5.7735862 Z"
	id="path1727"/>
 <path
	style="fill:#ffff00ff;stroke-width:0.26458332px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:0"
	d="M 3.0710565,7.1673734 C 4.2437886,7.9253194 4.8487355,7.3989306 5.5279017,6.8838911 5.847434,6.4830501 5.9642761,6.0484275 5.9058778,5.5845981 5.4155847,6.2005752 5.0449729,6.2689115 4.7955729,6.2224328 4.5976645,6.1668203 4.3199526,5.4727791 4.0632438,4.9467633 3.9849379,5.4507316 3.9142617,5.9546999 3.5907738,6.4586681 2.9420067,6.1890083 2.6937645,5.7591385 2.3387275,5.3719866 2.3244639,6.0169532 2.5176819,6.7471672 3.0710565,7.1673734 Z"
	id="path1729"/>
`,
	close: `
<path
	style="stroke-width:1.0583334;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
	d="M 0.66145834,0.64728424 7.843006,7.8524553"
	id="path814"/>
<path
	style="stroke-width:1.0583334;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1"
	d="M 7.8193823,0.64728422 0.63783482,7.8760788"
	id="path816"/>
`,
	star: `
<path
	style="opacity:1;fill-opacity:0;stroke-width:0.76199996;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1;paint-order:normal"
	id="path849"
	d="M 6.144003,6.1364431 4.7329341,5.4733016 4.2170123,6.9445924 3.6881478,5.4779041 2.2829761,6.1534521 2.9461177,4.7423831 1.4748268,4.2264613 2.9415152,3.6975968 2.2659672,2.2924252 3.6770362,2.9555667 4.192958,1.4842758 4.7218225,2.9509642 6.1269941,2.2754162 5.4638525,3.6864852 6.9351434,4.202407 5.4684551,4.7312715 Z" />
`,
	d20: `
<path
	style="fill:none;stroke-width:0.26458333;stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"
	d="M 1.6536458,1.4268601 4.7719493,0.49373139 5.7168896,1.6394717 6.8626302,5.7027158 2.9293156,4.5805989"
	id="path883"/>
<path
	style="fill:none;stroke-width:0.26458333;stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"
	d="M 6.8626301,5.7027156 3.5671503,7.958761 2.9293156,4.5805989"
	id="path885"/>
<path
	style="fill:none;stroke-width:0.26458333;stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"
	d="M 3.5671503,7.9587608 0.34254093,5.253869 2.9293156,4.5805989"
	id="path887"/>
<path
	style="fill:none;stroke-width:0.26458333;stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"
	d="M 0.34254093,5.253869 1.6536458,1.4268601"
	id="path889"/>
<path
	style="fill:none;stroke-width:0.26458333;stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"
	d="M 3.5671503,7.9587608 6.6027715,6.9783853 6.8626299,5.7027156 7.9256883,3.1041295 5.7168896,1.6394717"
	id="path891"/>
<path
	style="fill:none;stroke-width:0.26458333;stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"
	d="M 4.7719493,0.49373139 7.9256881,3.1041295"
	id="path893"/>
<path
	style="fill:none;stroke-width:0.26458333;stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"
	d="M 6.6027715,6.9783851 7.9256881,3.1041295"
	id="path895"/>
<path
	style="fill:none;stroke-width:0.26458333;stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"
	d="M 2.9293156,4.5805989 1.6536458,1.4268601 5.7168896,1.6394717"
	id="path897"/>
<path
	style="fill:none;stroke-width:0.26458333;stroke-linecap:round;stroke-linejoin:round;stroke-opacity:1;stroke-miterlimit:4;stroke-dasharray:none"
	d="M 2.9293156,4.5805989 5.7168896,1.6394717"
	id="path899"/>
`
};

class Icon extends HTMLElement {
	constructor() {
		super();
		this.isVisible = false;
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
		<link rel="stylesheet" href="/css/style.css" type="text/css"/>
		<span class="cs-icon"></span>
		`;
	}
	connectedCallback() {
		let iconName = this.getAttribute("icon-name");
		let iconSpan = this.shadowRoot.querySelector(`span.cs-icon`);
		let svgBody = icons[iconName];
		let size = this.getAttribute("size");
		size = parseInt(size);
		if (!size || isNaN(size)) {
			size = 16;
		}
		if (svgBody) {
			iconSpan.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="${size}" height="${size}" viewBox="0 0 8.45 8.45">${svgBody}</svg>`;
		}
		let altTxt = this.getAttribute("alternative-text");
		if (altTxt) {
			iconSpan.title = altTxt;
		}
	}
}
customElements.define("cs-icon", Icon);
