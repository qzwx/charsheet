const listeners = new Map();

function getListenerSet(eventName) {
	let listenersForEvt = listeners.get(eventName);
	if (listenersForEvt === undefined) {
		listenersForEvt = new Set();
		listeners.set(eventName, listenersForEvt);
	}
	return listenersForEvt;
}

function registerListener(eventName, listener) {
	getListenerSet(eventName).add(listener);
	//console.log(listeners);
}

function deregisterListener(eventName, listener) {
	getListenerSet(eventName).delete(listener);
	//console.log(listeners);
}

function fireEvent(eventName, payload) {
	//console.log("fireEvent", eventName, payload);
	for (let listener of getListenerSet(eventName)) {
		console.log("found listener for", eventName);
		try {
			listener(payload);
		} catch (err) {
			console.error(err);
		}
	}
}

export {registerListener, deregisterListener, fireEvent};
