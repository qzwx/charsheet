import "./modal.js";
import {show, hide, fixLabels, getElValue, setElValue} from "./util.js";
import {merge} from "./parser.js";

class SpellEdit extends HTMLElement {
	constructor() {
		super();
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
<link rel="stylesheet" href="/css/style.css" type="text/css"/>
<cs-modal class="edit-modal">
	<label slot="header" class="modal-label">Edit Spell</label>
	<div class="row mvxs center-items"><label class="col" data-for="name">Name</label><input class="col mh input" data-field="name" type="text"></input></div>
	<div class="row mvxs center-items"><label class="col" data-for="dc">DC</label><input class="col mh input" data-field="dc" type="text"></input></div>
	<div class="row mvxs center-items"><label class="col" data-for="time">Casting time</label><input class="col mh input" data-field="time" type="text"></input></div>
	<div class="row mvxs center-items"><label class="col" data-for="duration">Duration</label><input class="col mh input" data-field="duration" type="text"></input></div>
	<div class="row mvxs center-items"><label class="col" data-for="range">Range</label><input class="col mh input" data-field="range" type="text"></input></div>
	<div class="row mvxs center-items"><label class="col" data-for="components">Components</label><input class="col mh input" data-field="components" type="text"></input></div>
	<div class="row mvxs center-items"><label class="col" data-for="level">Level</label><input class="col mh input" data-field="level" type="number"></input></div>
	<div class="row mv center-items">
		<input class="input mh" data-field="ritual" type="checkbox"></input><label class="mh" data-for="ritual">Ritual</label>
		<input class="input mh" data-field="concentration" type="checkbox"></input><label class="mh" data-for="concentration">Concentration</label>
	</div>
	<div class="row mv center-items">
		<input class="input mh" data-field="prepared" type="checkbox"></input><label class="mh" data-for="prepared">Prepared</label>
	</div>
	<div class="row mvxs center-items">
		<label class="col" data-for="school">School</label>
		<select class="col mh input" data-field="school">
			<option>Abjuration</option>
			<option>Conjuration</option>
			<option>Divination</option>
			<option>Enchantment</option>
			<option>Evocation</option>
			<option>Illusion</option>
			<option>Necromancy</option>
			<option>Transmutation</option>
		</select>
	</div>
	<div><label data-for="description">Description</label></div>
	<div><textarea class="input" data-field="description" rows="10"></textarea></div>
	<div class="row mv">
		<button class="delete-btn col mhxs">Delete</button>
		<button class="cancel-btn col mhxs">Cancel</button>
		<button class="save-btn col mhxs">Save</button>
	</div>
</cs-modal>
		`;
		this.fixLabels = fixLabels.bind(this);
		this.getElValue = getElValue.bind(this);
		this.handleCancel = this.handleCancel.bind(this);
		this.handleDelete = this.handleDelete.bind(this);
		this.handleSave = this.handleSave.bind(this);
		this.openModal = this.openModal.bind(this);
		this.setElValue = setElValue.bind(this);
	}
	connectedCallback() {
		this.modal = this.shadowRoot.querySelector("cs-modal");
		this.modal.addEventListener("close", this.handleCancel);
		this.modalLabel = this.shadowRoot.querySelector("label.modal-label");
		this.shadowRoot
			.querySelector(".delete-btn")
			.addEventListener("click", this.handleDelete);
		this.shadowRoot
			.querySelector(".cancel-btn")
			.addEventListener("click", this.handleCancel);
		this.shadowRoot
			.querySelector(".save-btn")
			.addEventListener("click", this.handleSave);
		this.fixLabels();
	}

	handleError(err) {
		console.error(err);
		document.body.removeChild(this);
		this.reject(err);
	}
	init(values, resolve, reject) {
		this.resolve = resolve;
		this.reject = reject;
		try {
			this.editingId = values.id;
			if (values.mode === "edit") {
				this.modalLabel.textContent = "Edit Spell";
				show(this.shadowRoot.querySelector(".delete-btn"));
			} else {
				// if (values.mode === "new")
				this.modalLabel.textContent = "New Spell";
				hide(this.shadowRoot.querySelector(".delete-btn"));
			}
			let spell = values.spell;
			this.shadowRoot.querySelectorAll(".input").forEach((el) => {
				if (el.tagName === "INPUT" && el.type === "checkbox") {
					el.checked = !!spell[el.dataset.field];
				} else {
					let val = spell[el.dataset.field];
					el.value = val ? val : "";
				}
			});
		} catch (err) {
			this.handleError(err);
		}
	}
	handleSave() {
		try {
			let spell = {};
			const rv = {
				id: this.editingId,
				action: "save",
				spell
			};
			this.shadowRoot.querySelectorAll(".input").forEach((el) => {
				if (el.tagName === "INPUT" && el.type === "checkbox") {
					spell[el.dataset.field] = el.checked;
				} else {
					spell[el.dataset.field] = el.value;
				}
			});
			document.body.removeChild(this);
			this.resolve(rv);
		} catch (err) {
			this.handleError(err);
		}
	}
	handleDelete() {
		document.body.removeChild(this);
		this.resolve({
			id: this.editingId,
			action: "delete"
		});
	}
	handleCancel() {
		document.body.removeChild(this);
		this.resolve(null);
	}
	openModal() {
		this.modal.openModal();
		this.shadowRoot.querySelector('input.input[data-field="name"]').focus();
	}
}
customElements.define("cs-spell-edit", SpellEdit);

function showEditModal(values) {
	return new Promise((resolve, reject) => {
		let editModal = document.createElement("cs-spell-edit");
		document.body.appendChild(editModal);
		editModal.init(values, resolve, reject);
		editModal.openModal();
	});
}

export default showEditModal;
