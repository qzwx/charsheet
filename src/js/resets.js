import "./collapsible.js";
import {fireEvent} from "./event.js";

// TODO spinner?

export default class Resets extends HTMLElement {
	constructor() {
		super();
		this.attachShadow({mode: "open"});
		this.shadowRoot.innerHTML = `
		<link rel="stylesheet" href="/css/style.css" type="text/css"/>
		<cs-collapsible label="Resets" mode="open" noprint>
			<div class="row mv">
				<button class="btn col mhxs recalc-btn">Recalculate</button>
			</div>
			<div class="row mv">
				<button class="btn col mhxs reset-btn" data-reset-type="short-rest">Short Rest</button>
				<button class="btn col mhxs reset-btn" data-reset-type="long-rest">Long Rest</button>
				<button class="btn col mhxs reset-btn" data-reset-type="dawn">Dawn</button>
				<button class="btn col mhxs reset-btn" data-reset-type="dusk">Dusk</button>
			</div>
		</cs-collapsible>
		`;
		this.handleBtnClick = this.handleBtnClick.bind(this);
	}

	connectedCallback() {
		this.shadowRoot.querySelectorAll("button.reset-btn").forEach((btn) => {
			btn.addEventListener("click", this.handleBtnClick);
		});
		this.shadowRoot
			.querySelector("button.recalc-btn")
			.addEventListener("click", this.fireRecalcEvent);
	}

	handleBtnClick(evt) {
		let type = evt.target.dataset.resetType;
		fireEvent("reset", {type});
	}

	fireRecalcEvent() {
		console.log("fireRecalcEvent");
		fireEvent("recalc");
	}
}
customElements.define("cs-resets", Resets);
